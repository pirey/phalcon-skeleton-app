<?php
// php setPlatformState.php 1 -> pause platform
// php setPlatformState.php 0 -> play platform


require_once __DIR__ . '/init.php';

$ping = $di->get('redis')->ping();
if (!preg_match('/pong/i', $ping)) {
    echo date('Ymd_His').'Redis server has been gone!' . PHP_EOL;
    exit;
}

if (isset($argv[1])) {
    $redis->set(PLATFORM_PAUSED, intval($argv[1]));
    echo 'DONE, SET TO: ' . intval($argv[1]) .PHP_EOL;
}