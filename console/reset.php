<?php
require_once __DIR__ . '/cBootstrap.php';

// Set the start time
_log('Job started ###');
$connection = $di->get('dbMysql');

try {

    //Start a transaction
    $connection->begin();

    //Execute some SQL statements
    $connection->execute("UPDATE subscribers SET autocharge = 0 WHERE 1=1;");

    //Commit if everything goes well
    $connection->commit();
    _log("commit()");
    $logger->info('Autocharge Reset', 'autocharge');

} catch (Exception $e) {
    $connection->rollback();
    $logger->emergency($e->getMessage(), 'autocharge', null, null, $e);

    _log($e->getMessage());
    _log($e->getTraceAsString());
}

_log("Job finished ###");