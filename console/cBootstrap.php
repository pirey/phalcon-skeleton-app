<?php
// Set the time limit for php to 0 seconds
set_time_limit(0);

require_once __DIR__ . '/../app/boot.php';

$di = new Phalcon\DI\FactoryDefault();

# application config
$config = include ROOT . '/app/config/application.php';
$di->set('config', $config);

# autoloader
$loader = new Phalcon\Loader();
$loader->registerNamespaces(array(
    'App' => ROOT . '/app/library/App/',
));
$loader->register();

// multiDb
foreach ($config->databases as $db => $options) {
    $di->set($db, function () use ($options) {
        $adapter = @$options->adapter;
        unset($options->adapter);
        return new Phalcon\Db\Adapter\Pdo\Mysql($options->toArray());
    });
}

# redis
if (!in_array('redis', get_loaded_extensions())) {
    echo 'php-redis extension is not installed!';
    exit;
}

$redis = new Redis();
$redis->connect($config->redis->host, $config->redis->port);
$di->set('redis', $redis);

$di->set('notifier', function() {
    $notify = new \Tartan\Notification\Notifications();
    return $notify;
});

$di->set('logger', function () {
    return new \App\Model\Logs();
}, true);

## Checkups ###############################################
$ping = $di->get('redis')->ping();
if (!preg_match('/pong/i', $ping)) {
    $di->get('notifier')->add(
        $this->config->notification->developerId,
        'Redis server has been gone!',
        'development',
        'autocharge',
        '/error/'
    );

    $logger->emergency('Redis server has been gone!', 'autocharge');

    echo date('Ymd_His') . 'Redis server has been gone!' . PHP_EOL;
    exit;
}
## Helpers ################################################
function getServiceById ($id, $di)
{

    $serviceKey = 'service_' . $id;
    $service    = $di->get('redis')->get($serviceKey);

    if (!$service) {
        $service = $di->get('db')->fetchOne('SELECT * FROM services WHERE id = ' . intval($id), \Phalcon\Db::FETCH_OBJ);

        if (is_object($service)) {
            $di->get('redis')->set($serviceKey, json_encode($service));
        }
        // caching done
    }
    else {
        $service = json_decode($service);
    }

    return $service;
}


function _log($message)
{
    echo date('Ymd_His').' '.$message.PHP_EOL;
}