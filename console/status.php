<?php
require_once __DIR__ . '/cBootstrap.php';
/*
 * Simple CLI Script to Monitor Your Workers & Queues
 *
 * Usage: php status.php <delay in seconds>
 * Example: php status.php 5
 *
 */

echo "Creator Script Starting...\n";


echo "Connected to Redis Server \n";

$delay = $argv[1]; // Get how many jobs to create

if (!$delay) {
    $delay = 3; // Default delay of 3 seconds.
}

// Loop indefinitely
while (1) {
    // Get the status
    $queue_lengths         = array();
    $queue_lengths[QUEUE_INBOUNDS_TO_PROCESS]  = $di->get('redis')->llen(QUEUE_INBOUNDS_TO_PROCESS);
	$queue_lengths[QUEUE_INBOUNDS_TO_UPDATE_FLAG] = $di->get('redis')->llen(QUEUE_INBOUNDS_TO_UPDATE_FLAG);
	$queue_lengths[QUEUE_PROCESSED_INBOUNDS] = $di->get('redis')->llen(QUEUE_PROCESSED_INBOUNDS);
	$queue_lengths[QUEUE_OUTBOUNDS_TO_UPDATE_FLAG] = $di->get('redis')->llen(QUEUE_OUTBOUNDS_TO_UPDATE_FLAG);
	$queue_lengths[QUEUE_OUTBOUNDS_TO_OPERATOR] = $di->get('redis')->llen(QUEUE_OUTBOUNDS_TO_OPERATOR);

    $queue_total = 0;
    // Change null values to 0's
    foreach ($queue_lengths as $name => $size) {
        if ($size == null) {
            $queue_lengths[$name] = 0;
        }

        $queue_total += $queue_lengths[$name];
    }

    // Trim out old workers that haven't "worked" in over an hour
    $workers_time = $di->get('redis')->hgetall('worker.status.last_time');
    $time_limit   = time() - 60 * 60 * 1;

    foreach ($workers_time as $worker_id => $worker_ts) {
        if ($worker_ts < $time_limit) {
            $di->get('redis')->hdel('worker.status', $worker_id);
            $di->get('redis')->hdel('worker.status.last_time', $worker_id);
        }
    }

    $workers = $di->get('redis')->hgetall('worker.status');
    ksort($workers);

    // Display Queue status
    echo "\n--------------------------------------------\n";
    echo " Queue Statuses:\n\n";
    echo "    QUEUE_INBOUNDS_TO_PROCESS:  " . $queue_lengths[QUEUE_INBOUNDS_TO_PROCESS] . "\n";
    echo "    QUEUE_INBOUNDS_TO_UPDATE_FLAG: " . $queue_lengths[QUEUE_INBOUNDS_TO_UPDATE_FLAG]. "\n";
    echo "    QUEUE_PROCESSED_INBOUNDS: " . $queue_lengths[QUEUE_PROCESSED_INBOUNDS]. "\n";
    echo "    QUEUE_OUTBOUNDS_TO_UPDATE_FLAG: " . $queue_lengths[QUEUE_OUTBOUNDS_TO_UPDATE_FLAG]. "\n";
    echo "    QUEUE_OUTBOUNDS_TO_OPERATOR: " . $queue_lengths[QUEUE_OUTBOUNDS_TO_OPERATOR]. "\n";
    echo "    Total:    " . $queue_total . "\n\n";

    echo " Worker Statuses:\n\n";

    foreach ($workers as $worker_id => $status) {
        if ($worker_id > 0) {
            echo "	Worker [$worker_id]:	$status \n";
        }
    }

    echo "--------------------------------------------\n";

    // Sleep the delay
    sleep($delay);
}


