<?php
// Set the time limit for php to 0 seconds
set_time_limit(0);
require_once __DIR__ . '/cBootstrap.php';
/*
 * Worker That will Process "Jobs"
 */
$worker_id = $argv[1];

if (!$worker_id) {
    $worker_id = rand(100, 999);
}

echo "Worker [$worker_id] Starting...\n";

echo "Connecting to Redis Server... \n";

if (!in_array('redis', get_loaded_extensions())) {
    echo 'php-redis extension is not installed!';
    exit;
}

$inbounds  = new \App\Model\Inbounds();


$httpClient = new Zend\Http\Client();
$httpClient->setOptions(array(
    'maxredirects' => 0,
    'timeout'      => 10
));


$soapClient = new Zend\Soap\Client(
    $config->api->wsdl,
    ['compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_DEFLATE]
);
$context = stream_context_create(
    array(
        'http' => array(
            'timeout' => 15
        )
    )
);
$soapClient->setStreamContext($context);

// Setting the Worker's Status
$redis->hset('worker.status', $worker_id, 'Started');
$redis->hset('worker.status.last_time', $worker_id, time());

/*
 * We'll set our base time, which is one hour (in seconds).
 * Once we have our base time, we'll add anywhere between 0
 * to 10 minutes randomly, so all workers won't quick at the
 * same time.
 */
$time_limit = 60 * 60 * 1; // Minimum of 1 hour
$time_limit += rand(0, 60 * 10); // Adding additional time

// Set the start time
$start_time = time();

echo "Waiting for a Job..." . PHP_EOL;
$connection = $di->get('db');
$logger     = $di->get('logger');

// Continue looping as long as we don't go past the time limit
while (time() < $start_time + $time_limit)
{
    $state = $redis->get(PLATFORM_PAUSED);
    if ($state != 1)
    {
        //system paused != 1
        try {
            // Setting the Worker's Status
            $redis->hset('worker.status', $worker_id, 'Waiting');
            $redis->hset('worker.status.last_time', $worker_id, time());

            // Check to see if there are any items in the queues in
            // order of priority. If all are empty, wait up to 10
            // seconds for something to be added to the queue.
            $job = $redis->blpop(
                QUEUE_INBOUNDS_TO_PROCESS,
                QUEUE_INBOUNDS_TO_UPDATE_FLAG,
                QUEUE_PROCESSED_INBOUNDS,
                QUEUE_OUTBOUNDS_TO_OPERATOR,
                QUEUE_OUTBOUNDS_TO_UPDATE_FLAG,
                QUEUE_UPDATE_LAST_MESSAGE,
                10);

            // If a job was pulled out
            if ($job) {
                /*
                 * Start Working
                 *
                 * This is where you will use the data from the
                 */
                echo "Job Started " . PHP_EOL;
                // Setting the Worker's Status
                $redis->hset('worker.status', $worker_id, 'Working');
                $redis->hset('worker.status.last_time', $worker_id, time());

                $queueName = $job[0]; // 0 is the name of the queue
                $details   = unserialize($job[1]); // parse the json data from the job

                echo strtoupper('+++> Start to process [' . $queueName . '] job <+++') . PHP_EOL;
                echo print_r($details, true) . PHP_EOL;

                switch ($queueName) {

                    case QUEUE_INBOUNDS_TO_PROCESS: {
                        if ($details->invalid) {
                            _log('Process invalid message');

                            try
                            {
                                // get response from platform for invalid messages
                                $request = new Zend\Http\Request();
                                $uri = strtr ($config->application->invalidsUrl, [
                                    '{code}' => $details->invalid,
                                    '{shortcode}' => $details->shortcode
                                ]);
                                $request->setUri($uri);

                                $response = $httpClient->send($request);
                                $response = json_decode($response->getBody());
                            } catch (\Exception $e) {

                                if (isset($details->try)) {
                                    $details->try = $details->try + 1;
                                }
                                else {
                                    $details->try = 1;
                                }
                                _log('TRY COUNT:' . $details->try);

                                if ($details->try > 3) {
                                    # mark message as failed and
                                    $connection->execute("UPDATE inbounds SET failed = ? WHERE id = ?", [$details->try, $details->id]);
                                    # log service error
                                    $logger->error($e->getMessage(), 'QUEUE_INBOUNDS_TO_PROCESS', $details->id, $service->id, $e);
                                }
                                else {
                                    # return job to queue
                                    $jobSerial = serialize($details);
                                    $redis->rpush($queueName, $jobSerial);
                                }
                                _log('Exception :' . $e->getMessage());
                                break;
                            }

                            $arrayToInject = [
                                'fk_inbounds_id' => $details->id,
                                'msisdn'         => $details->msisdn,
                                'shortcode'      => $response->shortcode,
                                'content'        => $response->content,
                                'fk_services_id' => 0,
                                'mci_service_id' => 0,
                                'is_free'        => 1,
                                'created_at'     => date('Y-m-d H:i:s')
                            ];

                            try {
                                $outbounds = new \App\Model\Outbounds();
                                $outbounds->assign($arrayToInject);
                                $outbounds->create();
                            } catch (\PDOException $e) {
                                # mysql server has gone away >> should inject to outbound later
                                $di->get('redis')->rpush(QUEUE_PROCESSED_INBOUNDS, serialize($arrayToInject));
                                _log('PDOException :' . $e->getMessage());
                                $logger->emergency($e->getMessage(), 'QUEUE_INBOUNDS_TO_PROCESS', $details->id, $service->id, $e);
                            }
                        }
                        else {
                            //JOB without error
                            $service = getServiceById($details->fk_services_id, $di);

                            $serviceUri = $details->autocharge ? $service->ac_url : $service->od_url;
                            $uri        = strtr($serviceUri, [
                                '{inbound_id}' => $details->id,
                                '{msisdn}'     => $details->msisdn,
                                '{content}'    => urlencode($details->content),
                                '{service_id}' => $service->id,
                                '{time}'       => date('Ymd_His'),
                                '{shortcode}'  => $details->shortcode,
                                '{autocharge}' => $details->autocharge
                            ]);

                            _log('HTTP Call: ' . $uri);

                            try {
                                $request = new Zend\Http\Request();
                                $request->setUri($uri);
                                $request->getHeaders()->addHeaders(array(
                                    'APP_TOKEN' => $service->token
                                ));

                                $response = $httpClient->send($request);
                                $response = json_decode($response->getBody());
                            } catch (\Exception $e) {

                                if (isset($details->try)) {
                                    $details->try = $details->try + 1;
                                }
                                else {
                                    $details->try = 1;
                                }
                                _log('TRY COUNT:' . $details->try);

                                if ($details->try > 3) {
                                    # mark message as failed and
                                    $connection->execute("UPDATE inbounds SET failed = ? WHERE id = ?", [$details->try, $details->id]);
                                    # log service error
                                    $err                 = new App\Model\ServicesErrors();
                                    $err->bound          = 'inbound';
                                    $err->fk_bounds_id   = $details->id;
                                    $err->fk_services_id = $service->id;
                                    $err->error          = $e->getMessage();
                                    $err->details        = serialize($e);
                                    $err->save();
                                }
                                else {
                                    # return job to queue
                                    $jobSerial = serialize($details);
                                    $redis->rpush($queueName, $jobSerial);
                                }
                                _log('Exception :' . $e->getMessage());
                                break;
                            }

                            _log('HTTP result' . PHP_EOL . print_r($response, true));

                            $arrayToInject = [
                                'fk_inbounds_id' => $details->id,
                                'msisdn'         => $details->msisdn,
                                'shortcode'      => isset($response->shortcode) ? $response->shortcode : $details->shortcode,
                                'content'        => $response->content,
                                'fk_services_id' => $service->id,
                                'mci_service_id' => $service->mci_service_id,
                                'is_free'        => isset($response->is_free) ? $response->is_free : 0,
                                'created_at'     => date('Y-m-d H:i:s')
                            ];

                            try {
                                $outbounds = new \App\Model\Outbounds();
                                $outbounds->assign($arrayToInject);
                                $outbounds->create();
                            } catch (\PDOException $e) {
                                # mysql server has gone away >> should inject to outbound later
                                $di->get('redis')->rpush(QUEUE_PROCESSED_INBOUNDS, serialize($arrayToInject));
                                _log('PDOException :' . $e->getMessage());
                            }
                        }

                        break;
                    }
                    case QUEUE_INBOUNDS_TO_UPDATE_FLAG: {
                        try {
                            $connection->execute("UPDATE inbounds SET processed = 1 WHERE id IN (" . implode(',', $details) . ")");
                        } catch (Exception $e) {
                            $jobSerial = serialize($details);
                            $redis->rpush($queueName, $jobSerial);
                            _log('Exception :' . $e->getMessage());
                            $logger->emergency($e->getMessage(), $queueName, null, null, $details);
                        }
                        break;
                    }

                    case QUEUE_PROCESSED_INBOUNDS : {
                        try {
                            $outbounds = new \App\Model\Outbounds();
                            $outbounds->assign($details);
                            $outbounds->create();
                            break;
                        } catch (Exception $e) {
                            $di->get('redis')->rpush($queueName, serialize($details));
                            _log('PDOException :' . $e->getMessage());
                            $logger->emergency($e->getMessage(), $queueName, null, null, $details);
                        }
                        break;
                    }

                    case QUEUE_OUTBOUNDS_TO_UPDATE_FLAG: {
                        try {
                            $connection->execute("UPDATE outbounds SET processed = 1 WHERE id IN (" . implode(',', $details) . ")");
                        } catch (Exception $e) {
                            $jobSerial = serialize($details);
                            $redis->rpush($queueName, $jobSerial);
                            _log('Exception :' . $e->getMessage());
                            $logger->emergency($e->getMessage(), $queueName, null, null, $details);
                        }
                        break;
                    }

                    case QUEUE_OUTBOUNDS_TO_OPERATOR: {
                        $params = [
                            'username'          => $config->api->username,
                            'password'          => $config->api->password,
                            'numberList'        => [$details->msisdn],
                            'contentList'       => [$details->content],
                            'origShortCodeList' => [$details->shortcode],
                            'serviceIdList'     => [intval($details->mci_service_id)],
                            #'subscriberIdList'  => [1]
                        ];
                        //_log(print_r($params, true));


                        try {
                            $response = $soapClient->MessageListUploadWithServiceId($params);
                            if (is_object($response)) {
                                if (preg_match('/Success-\d+/i', $response->MessageListUploadWithServiceIdResult->string)) {
                                    _log('Got operator token');
                                    $refId = explode('-', $response->MessageListUploadWithServiceIdResult->string)[1];
                                    $connection->execute("UPDATE outbounds SET success_token = ? WHERE id = ?", [
                                        $refId,
                                        $details->id
                                    ]);
                                }
                                else {
                                    throw new Exception ("Soap response: {$response->MessageListUploadWithServiceIdResult->string}");
                                }
                            }
                            else {
                                throw new Exception ("Soap response: $response");
                            }
                        } catch (Exception $e) {
                            _log('Outbound Exception [' . $details->id . ']:' . $e->getMessage());
                            if (isset($details->try)) {
                                $details->try = $details->try + 1;
                            }
                            else {
                                $details->try = 1;
                            }
                            _log('TRY COUNT:' . $details->try);
                            if ($details->try >= 3) {
                                # mark message as failed and
                                $connection->execute("UPDATE outbounds SET failed = ? WHERE id = ?", [$details->try, $details->id]);
                                $logger->emergency($e->getMessage(), $queueName, $details->id, $details->fk_services_id, $details);
                            }
                            else {
                                $jobSerial = serialize($details);
                                $redis->rpush($queueName, $jobSerial);
                            }
                        }
                        break;
                    }

                    case QUEUE_UPDATE_LAST_MESSAGE: {
                        // job is array [msisdn, fk_Service_id]
                        try {
                            $connection->execute("UPDATE subscribers SET last_message = NOW() WHERE msisdn = ? AND fk_service_id = ?;", [$details['msisdn'], $details['fk_service_id']]);
                        } catch (Exception $e) {
                            $jobSerial = serialize($details);
                            $redis->rpush($queueName, $jobSerial);
                            $logger->emergency($e->getMessage(), $queueName, null, null, $e);
                        }
                        break;
                    }

                    default:
                        break;
                }

                echo strtoupper("+++> Done! Message from $queueName <+++\n");
                echo "Waiting for a Job..." . PHP_EOL;
            }
            else {
                echo ".";
            }
        } catch (\Exception $e) {
            // return failed job to queue
            $jobJson = json_encode($details);
            $redis->rpush($queueName, $jobJson);
            echo 'Exception :' . $e->getMessage() . PHP_EOL;
            $logger->emergency($e->getMessage(), $queueName, null, null, $e);
        }
    } // pause/play
}

// Setting the Worker's Status
$redis->hset('worker.status', $worker_id, 'Closed');
$redis->hset('worker.status.last_time', $worker_id, time());

echo "\n\nWorker [$worker_id] Finished! \nGoodbye...\n";
