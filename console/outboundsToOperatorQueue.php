<?php
require_once __DIR__ . '/cBootstrap.php';

$time_limit = 60 * 60 * 1; // Minimum of 1 hour
$time_limit += rand(0, 60 * 10); // Adding additional time

// Set the start time
$start_time = time();
$connection = $di->get('db');

while (time() < $start_time + $time_limit)
{
    $state = $redis->get(PLATFORM_PAUSED);
    if ($state != 1)
    {
        //system paused != 1

        $limit = $di->get('redis')->get(CONFIG_PICKER_LIMIT);
        $delay = $di->get('redis')->get(CONFIG_PICKER_DELAY);

        if (intval($limit) == 0) {
            $di->get('redis')->set(CONFIG_PICKER_LIMIT, 1000);
            $limit = 1000;
        }
        if (intval($delay) < 2) {
            $di->get('redis')->set(CONFIG_PICKER_DELAY, 5);
            $delay = 5;
        }

        if (date('s') % $delay == 0) {
            $outbounds = $connection->fetchAll("SELECT id, msisdn, content, shortcode, mci_service_id, fk_services_id FROM outbounds WHERE processed = 0 LIMIT " . $limit, Phalcon\Db::FETCH_OBJ);
            $ids       = [];
            $cnt       = 0;
            try {
                foreach ($outbounds as $message) {
                    $jobSerialized = serialize($message);
                    $redis->rpush(QUEUE_OUTBOUNDS_TO_OPERATOR, $jobSerialized);
                    $ids [] = $message->id;
                    $cnt++;
                }
            } catch (Exception $e) {
                if ($cnt > 0) {
                    _log("[" . implode(',', $ids) . "]");
                    _log(sprintf('Added %d item(s) to ' . QUEUE_OUTBOUNDS_TO_OPERATOR, $cnt));
                }
                if (isset($ids) && is_array($ids) && !empty($ids)) {
                    $result = $connection->execute("UPDATE outbounds SET processed = 1 WHERE id IN (" . implode(',', $ids) . ")");
                    _log('UPDATE inbounds result: ' . print_r($result, true));
                }
                _log($e->getMessage());
                echo $e->getTraceAsString() . PHP_EOL;
                $logger->error($e->getMessage(), 'outboundsToOperatorQueue', null, null, $e);
            }

            if ($cnt > 0) {
                _log("[" . implode(',', $ids) . "]");
                _log(sprintf('Added %d item(s) to ' . QUEUE_OUTBOUNDS_TO_OPERATOR, $cnt));
            }

            try {
                if (isset($ids) && is_array($ids) && !empty($ids)) {
                    $result = $connection->execute("UPDATE outbounds SET processed = 1 WHERE id IN (" . implode(',', $ids) . ")");
                    _log('UPDATE inbounds result: ' . print_r($result, true));
                }
            } catch (Exception $e) {
                $redis->rpush(QUEUE_OUTBOUNDS_THAT_PROCESSED, serialize($ids));
                _log($e->getMessage());
                echo $e->getTraceAsString() . PHP_EOL;
                $logger->error($e->getMessage(), 'outboundsToOperatorQueue', null, null, $e);
            }

            sleep(1);
        }
    } // pause/play
}

