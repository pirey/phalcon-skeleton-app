<?php
require_once __DIR__ . '/cBootstrap.php';

$time_limit = 60 * 60 * 1; // Minimum of 1 hour
$time_limit += rand(0, 60 * 10); // Adding additional time

// Set the start time
$start_time = time();
$connection = $di->get('db');
while (time() < $start_time + $time_limit)
{
    $state = $redis->get(PLATFORM_PAUSED);
    if ($state != 1)
    {
        // platform paused != 1

        $limit = $di->get('redis')->get(CONFIG_PICKER_LIMIT);
        $delay = $di->get('redis')->get(CONFIG_PICKER_DELAY);

        if (intval($limit) == 0) {
            $di->get('redis')->set(CONFIG_PICKER_LIMIT, 1000);
            $limit = 1000;
        }
        if (intval($delay) < 2) {
            $di->get('redis')->set(CONFIG_PICKER_DELAY, 5);
            $delay = 5;
        }

        if (date('s') % $delay == 0) {
            //_log(PHP_EOL.date('H:i:s'));
            $inbounds = $connection->fetchAll("SELECT * FROM inbounds WHERE processed = 0 AND failed = 0 LIMIT 6000", Phalcon\Db::FETCH_OBJ);
            $ids      = [];
            $cnt      = 0;
            try {
                foreach ($inbounds as $message) {
                    unset($message->raw_content);
                    unset($message->created_at);
                    unset($message->updated_at);
                    unset($message->shamsi_date);
                    $jobSerialized = serialize($message);
                    $redis->rpush(QUEUE_INBOUNDS_TO_PROCESS, $jobSerialized);

                    // UPDATE user's last message
                    $redis->rpush(QUEUE_UPDATE_LAST_MESSAGE, serialize([
                        'msisdn'        => $message->msisdn,
                        'fk_service_id' => $message->fk_services_id
                    ]));

                    $ids [] = $message->id;
                    $cnt++;
                }
            } catch (Exception $e) {
                if ($cnt) {
                    _log("[" . implode(',', $ids) . "]");
                    _log(sprintf('Added %d item(s) to ' . QUEUE_INBOUNDS_TO_PROCESS, $cnt));
                }

                if (isset($ids) && is_array($ids) && !empty($ids)) {
                    $result = $connection->execute("UPDATE inbounds SET processed = 1 WHERE id IN (" . implode(',', $ids) . ")");
                    _log('UPDATE inbounds result: ' . ($result ? 'TRUE' : 'FALSE'));
                }
                _log($e->getMessage());
                echo $e->getTraceAsString() . PHP_EOL;
                $logger->error($e->getMessage(), 'inboundsToProcessQueue', null, null, $e);
            }

            if ($cnt) {
                _log("[" . implode(',', $ids) . "]");
                _log(sprintf('Added %d item(s) to ' . QUEUE_INBOUNDS_TO_PROCESS, $cnt));
            }

            try {
                if (isset($ids) && is_array($ids) && !empty($ids)) {
                    $result = $connection->execute("UPDATE inbounds SET processed = 1 WHERE id IN (" . implode(',', $ids) . ")");
                    _log('UPDATE inbounds result: ' . ($result ? 'TRUE' : 'FALSE'));
                }
            } catch (Exception $e) {
                $redis->rpush(QUEUE_INBOUNDS_TO_UPDATE_FLAG, serialize($ids));
                _log($e->getMessage());
                echo $e->getTraceAsString() . PHP_EOL;
                $logger->error($e->getMessage(), 'inboundsToProcessQueue', null, null, $e);
            }
            sleep(1);
        } // pause/play
    }
}
