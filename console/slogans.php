<?php
require_once __DIR__ . '/cBootstrap.php';

    $services = $di->get('db')->fetchAll("SELECT name, free_shortcode, cost_shortcode, dedicated, slogan FROM services WHERE status = 1 AND slogan IS NOT NULL ORDER BY RAND() LIMIT 10", Phalcon\Db::FETCH_OBJ);
    $slogans  = [];

    foreach ($services as $service) {
        if (!empty($service->slogan)) {
            echo $service->slogan . ' ADDED' . PHP_EOL;
            $slogans [] = trim($service->slogan);
        }
    }

$di->get('redis')->set('slogans', serialize($slogans));
echo "DONE!\n";