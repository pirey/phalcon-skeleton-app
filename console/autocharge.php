<?php
require_once __DIR__ . '/cBootstrap.php';

if (isset($argv[1]) && is_numeric($argv[1])) {
    $serviceId = intval($argv[1]);
}
else {
    echo "Invalid parameters!" . PHP_EOL;
    echo "Smaple usage: php autocharge 1" . PHP_EOL;
    exit;
}

$service = \App\Model\Services::findFirst('id = ' . $serviceId);

// Set the start time
_log('Job started ###');
$connection = $di->get('db');

while (1) {

    $state = $redis->get(PLATFORM_PAUSED);
    if ($state != 1) {
        // platform paused != 1
        $limit = $di->get('redis')->get('config.outbound.limit');
        $delay = $di->get('redis')->get('config.outbound.delay');

        if (intval($limit) == 0) {
            $di->get('redis')->set('config.autocharge.limit', 1000);
            $limit = 1000;
        }

        if (intval($delay) < 2) {
            $di->get('redis')->set('config.autocharge.delay', 2);
            $delay = 2;
        }

        if (date('s') % $delay == 0) {
            $subscribers = $connection->fetchAll(
                sprintf("SELECT msisdn FROM subscribers WHERE flag = 1 AND autocharge = 0 AND fk_service_id = %d AND TIMESTAMPDIFF(MINUTE, last_message, NOW()) > 120 LIMIT %d", $serviceId, $limit),
                Phalcon\Db::FETCH_OBJ
            );

            $iq = 'INSERT INTO inbounds (msisdn, shortcode, raw_content, content, fk_services_id, created_at, autocharge) VALUES';

            $values = [];
            $ids    = [];

            foreach ($subscribers as $s) {
                echo date('Ymd_H:i').":{$service->id}:".$s->msisdn.PHP_EOL;
                $values [] = "('{$s->msisdn}', {$service->cost_shortcode}, '', '', {$service->id}, NOW(), 1)";
                $ids []    = $s->msisdn;
            }

            _log(count($ids) . " subscribers found.");
            if (count($ids) == 0) {
                break;
            }

            $iq .= implode(',', $values);
            $uq = "UPDATE subscribers SET autocharge = 1 WHERE fk_service_id = {$service->id} AND msisdn IN ('" . implode("','", $ids) . "')";

            if (count($ids)) {

                try {

                    //Start a transaction
                    $connection->begin();

                    //Execute some SQL statements
                    $connection->execute($iq);
                    $connection->execute($uq);

                    //Commit if everything goes well
                    $connection->commit();
                    _log("commit()");

                } catch (Exception $e) {
                    $connection->rollback();
                    _log($e->getMessage());
                    _log($e->getTraceAsString());
                }

            }
            sleep(1);
        } // pause/play
    }
    else {
        echo 'System paused! Nothing sent.' . PHP_EOL;
        exit;
    }
}
_log("Job finished ###");