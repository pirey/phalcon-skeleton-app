<?php
return [
    'Created at' => 'تاریخ ایجاد',
    'Expired at' => 'پایان اعتبار',
    'Home' => 'خانه',
    'Name' => 'نام',
    'Transactions' => 'تراکنشها',
    'User' => 'کاربر',
    'Error 103' => 'سر شماره غلط',
    'Error 105' => 'پیام خالی',
];
