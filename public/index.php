<?php

/**
 * @author Aboozar Ghafari <me@tartan.pro>
 */

require_once __DIR__ . '/../app/boot.php';
require_once ROOT . '/app/Bootstrap.php';
Bootstrap::run();