Banktest
===================
- Iranian Payment Gateways simulators
- Phalcon PHP Framework Application with Modular structure

Designed by Aboozar Ghaffari

Required
--------
- PHP 5.4 +
- php-intl 2.0 +
- Phalcon 1.3.3 +

Virtualhost sample
------------------
<VirtualHost *:80>

    ServerAdmin admin@banktest.ir
    DocumentRoot "/home/aboozar/www/banktest.ir/public"
    DirectoryIndex index.php
    ServerName banktest.ir
    ServerAlias www.banktest.ir

    <Directory "/home/aboozar/www/banktest.ir/public">
        Options All
        AllowOverride All
        Allow from all
        require all granted
        AddDefaultCharset UTF-8

        RewriteEngine On

        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteRule ^(.*)$ index.php?_url=/$1 [QSA,L]
    </Directory>

</VirtualHost>
