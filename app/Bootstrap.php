<?php
/**
 * Bootstrap
 * @author Aboozar Ghafari <a6oozar@gmail.com>
 */
class Bootstrap
{

    public static function run ()
    {
        $config = include __DIR__ . '/config/application.php';

        $di = new Phalcon\DI\FactoryDefault();
        $di->set('config', $config);

        $loader = new Phalcon\Loader();
        $loader->registerNamespaces(array(
            // Libraries
            'App'     => __DIR__ . '/library/App',
            'Tartan'  => __DIR__ . '/library/Tartan',
            // Modules
            'Site'    => __DIR__ . '/modules/Site',
            'Backend' => __DIR__ . '/modules/Backend',
            'Panel'   => __DIR__ . '/modules/Panel',
            'Gateway' => __DIR__ . '/modules/Gateway',
        ));
        $loader->register();

        $application = new Phalcon\Mvc\Application();

        $application->registerModules([
            'site'    => [
                'className' => 'Site\Module',
                'path'      => __DIR__ . '/modules/Site/Module.php'
            ],
            'backend' => [
                'className' => 'Backend\Module',
                'path'      => __DIR__ . '/modules/Backend/Module.php'
            ],
            'panel'   => [
                'className' => 'Panel\Module',
                'path'      => __DIR__ . '/modules/Panel/Module.php'
            ],
            'gateway'   => [
                'className' => 'Gateway\Module',
                'path'      => __DIR__ . '/modules/Gateway/Module.php'
            ]
        ]);

        $router = new Phalcon\Mvc\Router(false);
        $router->setDefaultModule('site');
        $router->setDefaultController('index');
        $router->setDefaultAction('index');

        $router->add('/:module/:controller/:action/:params', array(
            'module'     => 1,
            'controller' => 2,
            'action'     => 3,
            'params'     => 4
        ));

        $router->add("/auth/:action",
            array(
                "module"     => "site",
                "controller" => "auth",
                "action"     => 1,
            )
        );

        foreach ($application->getModules() as $module) {
            $routesClassName = str_replace('Module', 'Routes', $module['className']);
            if (class_exists($routesClassName)) {
                $routesClass = new $routesClassName();
                $router      = $routesClass->add($router);
            }
        }
        $router->removeExtraSlashes(true);

        $di->set('router', $router);

        $di->set('url', function () use ($config) {
            $url = new Phalcon\Mvc\Url();
            $url->setBaseUri($config->application->baseUri);
            $url->setStaticBaseUri($config->application->staticBaseUri);
            $url->setBasePath($config->application->basePath);
            return $url;
        }, true);

        // View
        $view = new Phalcon\Mvc\View();
        $view->setPartialsDir($config->view->partialsDir);
        $view->setLayoutsDir($config->view->layoutsDir);
        $view->setViewsDir($config->view->viewsDir);
        $view->setLayout($config->view->defaultLayout);
        $view->registerEngines(array(
            ".volt" => function ($view, $di) {
                $volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);
                $volt->setOptions(array(
                    'compiledPath' => ROOT . '/cache/volt/',
                    'compiledSeparator' => '_'
                ));

                return $volt;
            },
            ".phtml" => function ($view, $di) {
                $volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);
                $volt->setOptions(array(
                    'compiledPath' => ROOT . '/cache/volt/',
                    'compiledSeparator' => '_'
                ));

                return $volt;
            },
        ));
        $di->set('view', $view);

        // multiDb
        foreach ($config->databases as $db => $options) {
            $adapter = $options->adapter;
            unset($options->adapter);
            $foo =  new $adapter($options->toArray());
            $di->set($db, $foo);
        }

        // navigation
        $di->set('navigation', function () {
            $navNodes = include ROOT . '/res/nav.php';
            return new \Tartan\Navigation\Navigation($navNodes);
        }, true);

        // redis
        $redis = new Redis();
        $redis->connect($config->redis->host, $config->redis->port);
        $di->set('redis', $redis, true);

        $cache = function () use ($config, $redis) {
            $frontend = new Phalcon\Cache\Frontend\Data(array(
                'lifetime' => 3600,
                'prefix'   => $config->cache->prefix . '.'
            ));

            return new Phalcon\Cache\Backend\Redis($frontend, array(
                'redis' => $redis
            ));
        };

        $di->set('cache', $cache);
        $di->set('viewCache', $cache);
        $di->set('modelsCache', $cache);

        $di->set('modelsMetadata', function () use ($config, $redis) {
            return new \Phalcon\Mvc\Model\MetaData\Redis(array(
                "lifetime" => 3600,
                "prefix"   => $config->cache->prefix . ".metadata.",
                "redis"    => $redis
            ));
        });

        // Session
//        $di->setShared('session', function() {
//            $session = new Phalcon\Session\Adapter\Files();
//            $session->start();
//            return $session;
//        });
        $di->setShared('session', function () use ($config) {
            $session = new Phalcon\Session\Adapter\Redis(array(
                'lifetime' => 86400,
                'path'     => $config->redis->scheme . '://' . $config->redis->host . ':' . $config->redis->port . '?weight=1'
            ));

            $session->start();

            return $session;
        });

        $di->set('flash', function () {
            $flash = new Tartan\Flash\Direct(array(
                'error'   => 'alert alert-danger',
                'success' => 'alert alert-success',
                'notice'  => 'alert alert-info',
                'warning' => 'alert alert-warning'
            ));

            return $flash;
        });

        $di->set('flashSession', function () {
            $flash = new Tartan\Flash\Session(array(
                'error'   => 'alert alert-danger',
                'success' => 'alert alert-success',
                'notice'  => 'alert alert-info',
                'warning' => 'alert alert-warning'
            ));

            return $flash;
        });

        $assetsManager = new Phalcon\Assets\Manager();

        $assetsManager->collection('css')
            ->addCss('brio/css/bootstrap/bootstrap.css')
            ->addCss('brio/css/app/app.v1.css')
            ->setTargetPath($config->assets->dir . 'styles.css')
            ->setTargetUri('assets/styles.css')
            ->addFilter(new Phalcon\Assets\Filters\Cssmin())
            ->join(ENV == 'production');

        $assetsManager->collection('jsHeader')
            ->setTargetPath($config->assets->dir . 'jsheader.js')
            ->addFilter(new Phalcon\Assets\Filters\Jsmin())
            ->setTargetUri('assets/jsheader.js')
            ->addJs('brio/js/jquery/jquery-1.9.1.min.js')
            ->join(ENV == 'production');

        $assetsManager->collection('jsFooter')
            ->setTargetPath($config->assets->dir . 'jsfooter.js')
            ->setTargetUri('assets/jsfooter.js')
            ->addFilter(new Phalcon\Assets\Filters\Jsmin())
            ->addJs('brio/js/plugins/underscore/underscore-min.js')
            ->addJs('brio/js/bootstrap/bootstrap.min.js')
            ->addJs('brio/js/globalize/globalize.min.js')
            ->addJs('brio/js/plugins/nicescroll/jquery.nicescroll.min.js')
            ->addJs('brio/js/app/custom.js')
            ->join(ENV == 'production');

        $di->set('assets', $assetsManager);

        if (ENV == 'production')
        {
            $dispatcher = new \Phalcon\Mvc\Dispatcher();
            $eventsManager = new \Phalcon\Events\Manager();
            $eventsManager->attach("dispatch", function ($event, $dispatcher, $exception) use ($di)
            {
                if ($event->getType() == 'beforeNotFoundAction') {
                    $dispatcher->forward(array(
                        'module'     => 'site',
                        'controller' => 'error',
                        'action'     => 'error404'
                    ));

                    return false;
                }

                if ($event->getType() == 'beforeException') {
                    switch ($exception->getCode()) {
                        case \Phalcon\Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                        case \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                            $dispatcher->forward(array(
                                'module'     => 'site',
                                'controller' => 'error',
                                'action'     => 'error503'
                            ));

                            return false;
                    }
                }
            });
            $dispatcher->setEventsManager($eventsManager);

            $di->setShared('dispatcher', $dispatcher);
        }

        // $di->set('mail', array(
        //     'className' => 'Tartan\Mail',
        //     'arguments' => array(
        //         array('type' => 'service', 'name' => 'config'),
        //         //array('type' => 'parameter', 'value' => $config)
        //     )
        // ));

        $di->set('auth', function () {
            return new \Tartan\Auth();
        });

        $di->set('acl', function () {
            return new \Tartan\Acl();
        }, true);

        $di->set('tag', function () {
            return new \Tartan\Tag();
        }, true);

        $di->set('logger', function () {
            return new \App\Model\Logs();
        }, true);

        $di->set('t9n', function () use ($di) {
            $language = $di->get('session')->get("language");

            if (!$language) {
                $language = $di->get('request')->getBestLanguage();
                $di->get('session')->set("language", $language);
            }

            if (in_array($language, $di->get('config')->t9n->available->toArray())) {
                $translationPath = $di->get('config')->t9n->path . $language;
            } else {
                $translationPath = $di->get('config')->t9n->path . $di->get('config')->t9n->default;
            }

            $translates = include $translationPath . "/translation.php";

            $mainTranslate = new \Phalcon\Translate\Adapter\NativeArray([
                "content" => $translates
            ]);

            return $mainTranslate;
        });

        $di->set('notify', function() {
            $notify = new \Tartan\Notification\Notifications();
            return $notify;
        });

        $application->setDI($di);

        echo $application->handle()->getContent();
    }

}
