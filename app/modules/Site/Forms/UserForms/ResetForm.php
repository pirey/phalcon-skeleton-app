<?php
namespace Site\Forms\UserForms;

use App\Forms\Form,
    Phalcon\Forms\Element\Hidden,
    Phalcon\Forms\Element\Password,
    Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\Identical,
    Phalcon\Validation\Validator\StringLength,
    Phalcon\Validation\Validator\Confirmation;

class ResetForm extends Form
{

    public function initialize($entity=null, $options=null)
    {
        //Password
        $password = new Password('passwd', [
            'required' => true,
            'class' => 'ltr',
            'autofocus'=>'autofocus'
        ]);
        $password->setLabel('کلمه عبور');
        $password->addValidators(array(
            new PresenceOf(array(
                'message' => 'The password is required'
            )),
            new StringLength(array(
                'min' => 6,
                'messageMinimum' => 'Password is too short. Minimum 6 characters'
            )),
            new Confirmation(array(
                'message' => 'Password doesn\'t match confirmation',
                'with' => 'confirmPassword'
            ))
        ));

        $this->add($password);

        //Confirm Password
        $confirmPassword = new Password('confirmPassword', [
            'required' => true,
            'class' => 'ltr'
        ]);
        $confirmPassword->setLabel('تکرار کلمه عبور');
        $confirmPassword->addValidators(array(
            new PresenceOf(array(
                'message' => 'The confirmation password is required'
            ))
        ));
        $this->add($confirmPassword);

        //CSRF
        $csrf = new Hidden('csrf');
        $csrf->addValidator(
            new Identical(array(
                'value' => $this->security->getSessionToken(),
                'message' => 'CSRF validation failed'
            ))
        );
        $this->add($csrf);
    }
}