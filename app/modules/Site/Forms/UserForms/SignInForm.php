<?php
namespace Site\Forms\UserForms;

use App\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Hidden,
    Phalcon\Forms\Element\Password,
    Tartan\Forms\Element\Captcha,
    Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\Email,
    Phalcon\Validation\Validator\Identical,
    Phalcon\Validation\Validator\StringLength,
    Phalcon\Validation\Validator\Confirmation,
    Tartan\Validation\Validator\Captcha as CaptchaValidator;

class SignInForm extends Form
{

    public function initialize($entity=null, $options=null)
    {

        //Email
        $email = new Text('email', [
            'required' => true,
            'class' => 'form-control ltr',
            'autofocus'=>'autofocus'
        ]);
        $email->setLabel('ایمیل');

        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'The e-mail is required'
            )),
            new Email(array(
                'message' => 'The e-mail is not valid'
            ))
        ));

        $this->add($email);

        //Password
        $password = new Password('password', [
            'required' => true,
            'class' => 'form-control ltr'
        ]);
        $password->setLabel('کلمه عبور');
        $password->addValidators(array(
            new PresenceOf(array(
                'message' => 'The password is required'
            )),
            new StringLength(array(
                'min' => 6,
                'messageMinimum' => 'Password is too short. Minimum 6 characters'
            ))
        ));

        $this->add($password);

        //Captcha
        $captcha = new Captcha('captcha', [
            'class' => 'form-control ltr col-lg-6',
            'required' => true
        ]);
        $captcha->setLabel('کد امنیتی');
        $captcha->addValidators(array(
            new PresenceOf(array(
                'message' => 'The captcha is required'
            )),
            new CaptchaValidator()
        ));
        $captcha->setUserOption('refresh-message', 'برای تغییر کد امنیتی اینجا کلیک کنید');
        $this->add($captcha);


        //CSRF
        $csrf = new Hidden('csrf');
        $csrf->addValidator(
            new Identical(array(
                'value' => $this->security->getSessionToken(),
                'message' => 'CSRF validation failed'
            ))
        );
        $this->add($csrf);
    }
}