<?php
namespace Site\Forms\UserForms;

use App\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Hidden,
    Tartan\Forms\Element\Captcha,
    Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\Email,
    Phalcon\Validation\Validator\Identical,
    Tartan\Validation\Validator\Captcha as CaptchaValidator;

class ForgotForm extends Form
{

    public function initialize($entity=null, $options=null)
    {

        //Email
        $email = new Text('email', [
            'required' => true,
            'class' => 'ltr',
            'autofocus'=>'autofocus'
        ]);
        $email->setLabel('ایمیل');

        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'The e-mail is required'
            )),
            new Email(array(
                'message' => 'The e-mail is not valid'
            ))
        ));

        $this->add($email);


        //Captcha
        $captcha = new Captcha('captcha', [
            'required' => true
        ]);
        $captcha->setLabel('کد امنیتی');
        $captcha->addValidators(array(
            new PresenceOf(array(
                'message' => 'The captcha is required'
            )),
            new CaptchaValidator()
        ));
        $this->add($captcha);


        //CSRF
        $csrf = new Hidden('csrf');
        $csrf->addValidator(
            new Identical(array(
                'value' => $this->security->getSessionToken(),
                'message' => 'CSRF validation failed'
            ))
        );
        $this->add($csrf);
    }
}