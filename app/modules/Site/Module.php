<?php

/**
 * @author Aboozar Ghafari <a6oozar@gmail.com>
 */

namespace Site;

use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Loader;

class Module implements ModuleDefinitionInterface
{

    public function registerAutoloaders()
    {
    /*
        $loader = new Loader();

        $loader->registerNamespaces(
            array(
                'Site\Controller' => __DIR__ . '/controller/',
                'Site\Model'      => __DIR__ . '/model/',
            )
        );

        $loader->register();
    */
    }

    public function registerServices($di)
    {
        $dispatcher = $di->get('dispatcher');
        $dispatcher->setDefaultNamespace('Site\Controller');
        $di->set('dispatcher', $dispatcher);

        $view = $di->get('view');
        $view->setViewsDir(__DIR__ . '/views/');
        $view->setPartialsDir('../../../views/partials/');
        $di->set('view', $view);
    }

}
