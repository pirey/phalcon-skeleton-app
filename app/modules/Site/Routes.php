<?php

/**
 * Routes
 * @author Aboozar Ghafari <me@tartan.pro>
 */

namespace Site;

class Routes
{

    public function add ($router)
    {
        // Homepage router
        $router->add('/', array(
            'module'     => 'site',
            'controller' => 'index',
            'action'     => 'index',
        ))->setName('home');

        $router->add("/site", array(
            'module'     => 'site',
            'controller' => 'index',
            'action'     => 'index',
        ));

        $router->add('/site/:controller', array(
            'module'     => 'site',
            'controller' => 1,
            'action'     => 'index',
        ));

        $router->add('/site/:controller/:action', array(
            'module'     => 'site',
            'controller' => 1,
            'action'     => 2,
        ));
        $router->add(
            "/site/:controller/:action/:params",
            array(
                'module'     => 'site',
                "controller" => 1,
                "action"     => 2,
                "params"     => 3,
            )
        );

        // Named Routes ================================================================================================
        $router->add('/user/dashboard', array(
            'module'     => 'panel',
            'controller' => 'member',
            'action'     => 'index',
        ))->setName('dashboard');

        $router->add("/user/signup", array(
            'module'     => 'site',
            'controller' => 'user',
            'action'     => 'signup',
        ))->setName('signup');

        $router->add("/user/signin", array(
            'module'     => 'site',
            'controller' => 'user',
            'action'     => 'signin',
        ))->setName('signin');

        $router->add("/user/signout", array(
            'module'     => 'site',
            'controller' => 'user',
            'action'     => 'signout',
        ))->setName('signout');

        $router->add("/user/forgotpassword", array(
            'module'     => 'site',
            'controller' => 'user',
            'action'     => 'forgotpassword',
        ))->setName('forgotPassword');

        $router->add("/user/resendActivation", array(
            'module'     => 'site',
            'controller' => 'user',
            'action'     => 'resendactivation',
        ))->setName('resendActivation');

        $router->add("/user/resetpassword/{code:[A-Za-z0-9]+}", array(
            'module'     => 'site',
            'controller' => 'user',
            'action'     => 'resetPassword',
        ))->setName('resetPassword');

        $router->add("/user/activation/{code:[A-Za-z0-9]+}", array(
            'module'     => 'site',
            'controller' => 'user',
            'action'     => 'activation',
        ))->setName('userActivation');

        $router->add("/captcha", array(
            'module'     => 'site',
            'controller' => 'captcha',
            'action'     => 'index',
        ))->setName('captcha');

        $router->add(
            "/authorize/oauth/{provider:[A-Za-z]+}",
            array(
                'module'     => 'site',
                'controller' => 'oauth',
                'action'     => 'login'
            )
        )->setName('oauth');

        return $router;
    }
}