<section id="content">
    <div class="bg-dark lt">
        <div class="container">
            <div class="m-b-lg m-t-lg">
                <h3 class="m-b-none">
                    {% if title is defined %}
                        {{ title }}
                    {% endif %}
                </h3>
                <small class="text-muted">
                    {% if pagedesc is defined %}
                        {{ pagedesc }}
                    {% endif %}
                   </small>
            </div>
        </div>
    </div>
    <div class="bg-white b-b b-light">
        <div class="container">
            <ul class="breadcrumb no-border bg-empty m-b-none m-l-n-sm">
                <li><a href="/">{{i18n._('Home')}}</a></li>
                {% for text, link in breadcrumbs %}
                {% if link is empty %}
                    {% set link = "#" %}
                {% endif %}
                {% if loop.last %}
                <li>{{ link_to(link, i18n._(text), 'class':'active') }}</li>
                {% else %}
                <li>{{ link_to(link, i18n._(text)) }}</li>
                {% endif %}
                {% endfor %}
            </ul>
        </div>
    </div>
    <div>
        <div class="container m-t-md">
            <?php $this->flashSession->output() ?>
            {{ content() }}
        </div>
    </div>
</section>