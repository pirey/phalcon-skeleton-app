<?php
namespace Site\Controller;

use Phalcon\Mvc\Controller;
use Tartan\Captcha\Captcha;

class CaptchaController extends Controller
{
    public function initialize()
    {
        parent::initialize();
        $this->view->disable();
    }
    public function indexAction()
    {
        $captcha = new Captcha();
        $captcha->CreateImage();
    }
}
