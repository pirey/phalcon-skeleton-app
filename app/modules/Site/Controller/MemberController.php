<?php

/**
 * @author Aboozar Ghafari <a6oozar@gmail.com>
 */

namespace Site\Controller;

use Tartan\Mvc\Controller;

class MemberController extends Controller
{

    public function dashboardAction()
    {
       exit('site::member::dashboard');
    }
}
