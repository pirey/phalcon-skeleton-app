<?php
namespace Site\Controller;

use Tartan\Mvc\Controller;
use Phalcon\Mvc\View;
use Tartan\Auth\Exception as AuthException;
use App\Model\Members;
use League\OAuth2\Client\Provider\Github;
use League\OAuth2\Client\Provider\Facebook;
use League\OAuth2\Client\Provider\Google;
use League\OAuth2\Client\Provider\Microsoft;


class OauthController extends Controller
{
    public function loginAction ()
    {
        $providerName = ucfirst(strtolower($this->dispatcher->getParam("provider")));

        $className = "League\\OAuth2\\Client\\Provider\\{$providerName}";
        if (!class_exists($className)) {
            echo 'provider not found';
            exit;
        }

        $config    = $this->config->oAuth->{$providerName};
        $publicURL = $this->url->get(array(
                'for'      => 'oauth',
                'provider' => strtolower($providerName)
            ));

        $provider = new $className([
            'clientId'     => $config->id,
            'clientSecret' => $config->secret,
            'redirectUri'  => $publicURL
        ]);

        if (!$this->request->getQuery('code')) {

            // If we don't have an authorization code then get one
            header('Location: ' . $provider->getAuthorizationUrl());
            exit;

        }
        else {

            // Try to get an access token (using the authorization code grant)
            $token = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);


            // If you are using Eventbrite you will need to add the grant_type parameter (see below)
//            $token = $provider->getAccessToken('authorization_code', [
//                'code' => $_GET['code'],
//                'grant_type' => 'authorization_code'
//            ]);

            // Optional: Now you have a token you can look up a users profile data
            try {

                // We got an access token, let's now get the user's details
                $userEntity = $provider->getUserDetails($token);

                return $this->auth->loginWithOAuth2($userEntity, $providerName);

            } catch (AuthException $e) {
                $this->flashSession->error($e->getMessage());

                return $this->response->redirect(['for' => 'signin']);
            }
        }
    }
}
