<?php

/**
 * ErrorController
 * @copyright Copyright (c) 2011 - 2013 Bazaresham
 * @author Aboozar Ghafari <a6oozar@gmail.com>
 */

namespace Site\Controller;

use Phalcon\Mvc\Controller;

class ErrorController extends Controller
{

    public function error404Action()
    {
        exit('ERROR 404');
    }

    public function error503Action()
    {
        exit('ERROR 503');
    }
}
