<?php

namespace Site\Controller;

use Tartan\Mvc\Controller;
use OAuth\Common\Service\AbstractService;
use OAuth\Common\Consumer\Credentials as OAuthCredentials;
use OAuth\Common\Storage\Session as  OAuthSession;

class AuthController extends Controller
{

    // everytime we enter the controller, then we check for login, if yes, then we dont have to access here (except logout)
//    public function beforeExecuteRoute($dispatcher){
//
//
//        if ($dispatcher->getActionName() == 'logout') {
//            return true;
//        }
//
////        $isLogged = ...; // specific to your application
////        if($isLogged){
////            return $this->response->redirect();
////        }
//
//        return true;
//    }

    /**
     * @param $serviceName string name of the service
     * @return bool|\OAuth\ServiceFactory the service ready to be requested or false if something went wrong
     */
    protected function getOAuthService($serviceName,$storage){

        $oAuthCredentials = $this->getDI()->get("config")["oAuth"];

        // the auth service must be registered in config
        if(!isset($oAuthCredentials[$serviceName])){
            return false;
        }

        $uriFactory = new \OAuth\Common\Http\Uri\UriFactory();
        $currentUri = $uriFactory->createFromSuperGlobalArray($_SERVER);
        $currentUri->setQuery('');

        $credentials = new OAuthCredentials(
            $oAuthCredentials[$serviceName]['key'],
            $oAuthCredentials[$serviceName]['secret'],
            $currentUri->getAbsoluteUri()
        );

        $serviceFactory = new \OAuth\ServiceFactory();
        $service = $serviceFactory->createService($serviceName, $credentials, $storage);

        return $service;
    }

    protected function proceedOAuth($serviceName){
        $authService = $this->di->get("auth");

        $storage = new OAuthSession();
        $storage->clearAllTokens();

        $service = $this->getOAuthService($serviceName,$storage);


        // make sure service is ok
        if(!$service){
            return $this->dispatcher->forward("error","notFound"); //specific to your application
        }

        if ( $this->request->hasQuery("code") ) {
            $code =  $this->request->getQuery("code");
            if( empty($code) ){
                $this->flash->error("A problem occurred with $serviceName");
                return $this->dispatcher->forward("error","fatal");//specific to your application
            }

            try{
                $service->requestAccessToken($code);
            }catch (\OAuth\Common\Http\Exception\TokenResponseException $e){
                return null;
            }

            return $service;
        }else{

            $url = $service->getAuthorizationUri();
            $this->response->redirect($url,true)->send();
            return null;

        }

    }

    public function githubAction(){
        // connect with github oAuth
        $service = $this->proceedOAuth("GitHub");

        // when connected, then we access this point
        // and we can query github api
        try{
            $result = json_decode($service->request('user'), true);
        }catch(\Exception $e){
            //...
        }
    }

    public function googleAction(){

        // connect with google oAuth
        $service = $this->proceedOAuth("google");

        // when connected, then we access this point
        // and we can query google api
        try{
            $result = json_decode($service->request('https://www.googleapis.com/oauth2/v1/userinfo'), true);
        }catch(\Exception $e){
            //...
        }
    }
}