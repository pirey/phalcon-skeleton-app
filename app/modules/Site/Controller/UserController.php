<?php
namespace Site\Controller;

use Phalcon\Mvc\View;
use Tartan\Mail\Message as TartanMailMessage;
use Tartan\Auth\Exception as AuthException;
use Tartan\Mvc\Controller;
use Tartan\Auth\Model\Members;
use Tartan\Auth\Model\TempCodes;

class UserController extends Controller
{
    public function indexAction ()
    {
        if ($this->auth->hasIdentity()) {
            return $this->response->redirect(['for' => 'dashboard']);
        }
        else {
            return $this->response->redirect(['for' => 'signin']);
        }
    }

    public function signupAction ()
    {
        $this->tag->setTitle("Sign up");
        $this->view->breadcrumbs = [
            'User'    => '',
            'Sign up' => ['for' => 'signup']
        ];
        $form = new \Site\Forms\UserForms\SignUpForm();

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost()) != false) {
                $member = new Members();

                $member->assign(array(
                    'name'      => $this->request->getPost('name', 'striptags'),
                    'identity'  => $this->request->getPost('email', 'email'),
                    'password'  => $this->request->getPost('password')
                ));

                if ($member->save()) {
                    $this->flashSession->success('You\'ve signed up successfully. Please check your mail for activation message.');

                    return $this->response->redirect(['for' => 'signin'], true);
                }
                else {
                    foreach ($member->getMessages() as $message) {
                        $this->flashSession->error($message);
                    }
                }
            }
        }

        $this->view->form = $form;
    }

    public function signinAction ()
    {
        $this->tag->setTitle("Sign in");
        $this->view->breadcrumbs = [
            'User'    => '',
            'Sign in' => ['for' => 'signin']
        ];
        $form = new \Site\Forms\UserForms\SignInForm();

        try {
            if (!$this->request->isPost()) {
                if ($this->auth->hasRememberMe()) {
                    return $this->auth->loginWithRememberMe();
                }
            }
            else { // POST request
                if ($form->isValid($this->request->getPost()) != false) {
                    $this->auth->loginNormal(array(
                        'email'    => $this->request->getPost('email'),
                        'password' => $this->request->getPost('password'),
                        'remember' => $this->request->getPost('remember')
                    ));

                    // redirected via Auth
                    //return $this->response->redirect(array('for' => 'dashboard'));
                }
            }
        } catch (AuthException $e) {
            $this->flashSession->error($e->getMessage());
        }

        $this->view->form = $form;
    }

    public function signoutAction ()
    {
        $this->auth->remove();

        return $this->response->redirect(['for' => 'home']);
    }

    public function activationAction ()
    {
        $this->tag->setTitle("User activation");
        $this->view->breadcrumbs = [
            'User'       => '',
            'Activation' => ['for' => 'userActivation']
        ];

        $code     = $this->dispatcher->getParam('code', 'alphanum');
        $tempCode = TempCodes::findFirst(sprintf("code = '%s' AND reason = '%s'", $code, TempCodes::ACTIVE));

        $route = 'home'; // default redirect route

        if ($tempCode)
        {
            $now    = new \DateTime('now');
            $expire = new \DateTime($tempCode->getExpiredAt());

            if ($now > $expire) {
                $this->flashSession->error('Code is expired!');
                $route = 'home';
            }
            else {
                $member = Members::findFirst($tempCode->getElementId());
                if ($member->getDeleted()) {
                    $this->flashSession->error('Blocked account! Please contact administrator');
                }
                elseif ($member->getBanned()) {
                    $this->flashSession->error('Banned account! Please contact administrator');
                }
                elseif ($member->getActive()) {
                    $this->flashSession->error('Your account is active now and activation does not required.');
                }
                else {
                    // activate account
                    $member->setActive(1);
                    if ($member->save()) {
                        $this->flashSession->success('Your account activated.');
                        $route = 'signin';
                    }
                    else {
                        //foreach ($member->getMessages() as $message) {
                        //    $this->flashSession->error($message);
                        //}
                        $this->flashSession->error('Error during updating user status');
                    }
                }
            }

            $tempCode->delete(); // delete temp code
        }
        else {
            $this->flashSession->error('invalid confirm code');
            $route = 'home';
        }

        return $this->response->redirect(['for' => $route], true);
    }

    public function resendActivationAction ()
    {
        $this->view->title       = "Resend activation email";
        $this->view->breadcrumbs = [
            'User'                    => '',
            'Resend activation email' => ['for' => 'resendActivation']
        ];
        $form = new \Site\Forms\UserForms\ResendForm();

        $route = 'home';

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost()) != false) {
                $member = Members::findFirst([
                    'email = :email:',
                    'bind' => ['email' => $this->request->getPost('email', 'email')]
                ]);

                if ($member->getDeleted()) {
                    $this->flashSession->error('Blocked account! Please contact administrator');
                }
                elseif ($member->getBanned()) {
                    $this->flashSession->error('Banned account! Please contact administrator');
                }
                elseif ($member->getActive()) {
                    $this->flashSession->error('Your account is active now and activation does not required.');
                }
                else {
                    $lastResend = $member->getStatus();
                    $lastResend = empty($lastResend) ? 0 : strtotime($lastResend);

                    if ((time() - $lastResend) < (24 * 60 * 60)) {
                        $this->flashSession->warning('We`ve been sent you an activation mail today. Please try 24 hours later.');
                    }
                    else {
                        $member->sendActivationCode();
                        $member->setStatus(date('Y-m-d H:i:s'))->save();
                        $this->flashSession->success('Activation message has been sent. please check your mail again.');
                    }
                }
            }
        }

        $this->view->form = $form;
    }

    public function forgotPasswordAction ()
    {
        $this->tag->setTitle("Forgot password");

        $this->view->breadcrumbs = [
            'User'                    => '',
            'Resend activation email' => ['for' => 'forgotPassword']
        ];
        $form = new \Site\Forms\UserForms\ForgotForm();

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost()) != false) {
                $member = Members::findFirst([
                    'email = :email:',
                    'bind' => ['email' => $this->request->getPost('email', 'email')]
                ]);

                if ($member instanceof Members) {
                    $tempCode = new TempCodes();
                    $code     = $tempCode->generateCode($member->getId(), TempCodes::FORGOT);

                    $m = new TartanMailMessage();

                    $m->setSubject('Forgot Password')
                        ->setTemplate('forgotPassword')
                        ->setTemplateParameters([
                            'resetUrl' => $this->url->get([
                                'for'  => 'resetPassword',
                                'code' => urlencode($code)
                            ]),
                            'code'     => $code
                        ])->addTo($member->email, $member->name);

                    $this->mail->send($m);

                    $this->flashSession->success('Reset password mail has been sent. Please check your mail again.');
                }
                else {
                    $this->flashSession->error('Email not found.');
                }
            }
        }

        $this->view->form = $form;
    }

    public function resetPasswordAction ()
    {
        $this->tag->setTitle("Reset password");
        $this->view->breadcrumbs = [
            'User'                    => '',
            'Resend activation email' => ['for' => 'resetPassword']
        ];
        $code = $this->dispatcher->getParam('code', 'alphanum');
        $tempCode = TempCodes::findFirst(sprintf("code = '%s' AND reason = '%s'", $code, TempCodes::FORGOT));

        $route = 'home'; // default redirect route

        if ($tempCode) {
            $now    = new \DateTime('now');
            $expire = new \DateTime($tempCode->getExpiredAt());

            if ($now > $expire) {
                $this->flashSession->error('code expired!');
            }
            else {
                $member = Members::findFirst($tempCode->getElementId());

                if ($member->getDeleted()) {
                    $this->flashSession->error('Blocked account! Please contact administrator');
                }
                elseif ($member->getBanned()) {
                    $this->flashSession->error('Banned account! Please contact administrator');
                }
                else {
                    $member->setActive(1);
                    if ($member->save()) {
                        $tempCode->delete(); // Removed used code

                        $this->flashSession->success('Your account activated.');
                        $route = 'signin';
                    }
                    else {
                        /**
                         * @todo log the member model error(s)
                         */
                        //foreach ($member->getMessages() as $message) {
                        //    $this->flashSession->error($message);
                        //}
                        $this->flashSession->error('Error during updating user information. please contact administrator');
                    }
                }
            }
            // delete code
            //$tempCode->delete();
        }
        else {
            $this->flashSession->error('Invalid reset password code!');
        }

        return $this->response->redirect(['for' => $route], true);
    }
}