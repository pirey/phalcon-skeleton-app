<?php
namespace Panel\Controller;

use App\Mvc\Controller;

class LiveController extends Controller
{
    public function initialize ()
    {
        parent::initialize();
        $this->view->disable();
        header('Content-Type: application/json');
    }

    public function queueAction ()
    {
        // Get the status
        $queue_lengths                                 = array();
        $queue_lengths[QUEUE_INBOUNDS_TO_PROCESS]      = $this->redis->llen(QUEUE_INBOUNDS_TO_PROCESS);
        $queue_lengths[QUEUE_UPDATE_LAST_MESSAGE]      = $this->redis->llen(QUEUE_UPDATE_LAST_MESSAGE);
        $queue_lengths[QUEUE_INBOUNDS_TO_UPDATE_FLAG]  = $this->redis->llen(QUEUE_INBOUNDS_TO_UPDATE_FLAG);
        $queue_lengths[QUEUE_PROCESSED_INBOUNDS]       = $this->redis->llen(QUEUE_PROCESSED_INBOUNDS);
        $queue_lengths[QUEUE_OUTBOUNDS_TO_UPDATE_FLAG] = $this->redis->llen(QUEUE_OUTBOUNDS_TO_UPDATE_FLAG);
        $queue_lengths[QUEUE_OUTBOUNDS_TO_OPERATOR]    = $this->redis->llen(QUEUE_OUTBOUNDS_TO_OPERATOR);

        $queue_total = 0;
        // Change null values to 0's
        foreach ($queue_lengths as $name => $size) {
            if ($size == null) {
                $queue_lengths[$name] = 0;
            }

            $queue_total += $queue_lengths[$name];
        }

        // Trim out old workers that haven't "worked" in over an hour
        $workers_time = $this->redis->hgetall('worker.status.last_time');
        $time_limit   = time() - 60 * 60 * 1;

        foreach ($workers_time as $worker_id => $worker_ts) {
            if ($worker_ts < $time_limit) {
                $this->redis->hdel('worker.status', $worker_id);
                $this->redis->hdel('worker.status.last_time', $worker_id);
            }
        }

        $workers = $this->redis->hgetall('worker.status');
        ksort($workers);

        $queue_lengths ['total'] = $queue_total;

        echo json_encode($queue_lengths);
    }

    public function mysqlHealthAction()
    {
        $statuses = [];

        try {
            $r = (int) is_object($this->db->query('SELECT 1+1'));
        } catch (\PDOException $e) {
            $r = 0;
        }

        $statuses ['mysql'] = $r;

        try {
            $ping = $this->redis->ping();
            if (!preg_match('/pong/i', $ping)) {
                $r = 1;
            }
        } catch (\Exception $e) {
            $r = 0;
        }

        $statuses ['redis'] = $r;

        echo json_encode($statuses);
    }

    public function redisHealthAction()
    {
        try {
            $ping = $this->redis->ping();
            if (!preg_match('/pong/i', $ping)) {
                $r = 1;
            }
        } catch (\Exception $e) {
            $r = 0;
        }

        echo json_encode(['status' => $r]);
    }
}