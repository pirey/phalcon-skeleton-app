<?php

/**
 * @author Aboozar Ghafari <a6oozar@gmail.com>
 */

namespace Panel\Controller;

use App\Mvc\Controller;

class SettingController extends Controller
{
    public function indexAction ()
    {
        $this->view->pageTitle = 'Platform Settings';
        $this->view->platformStatus = $this->redis->get(PLATFORM_PAUSED);

        $this->view->pickerLimit = $this->redis->get(CONFIG_PICKER_LIMIT);
        $this->view->pickerDelay = $this->redis->get(CONFIG_PICKER_DELAY);
    }

    public function togglePlatformStatusAction()
    {
        $platformStatus = $this->redis->get(PLATFORM_PAUSED);
        $this->redis->set(PLATFORM_PAUSED, ($platformStatus == 1 ? 0 : 1));
        return $this->response->redirect('panel/setting');
    }
}