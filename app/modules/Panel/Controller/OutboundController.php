<?php

/**
 * @author Aboozar Ghafari <a6oozar@gmail.com>
 */

namespace Panel\Controller;
use Phalcon\Paginator\Adapter\NativeArray as ArrayPaginator;
use Phalcon\Paginator\Adapter\QueryBuilder as Paginator;

use App\Mvc\Controller;
use App\Model\Outbounds;

class OutboundController extends Controller
{

    public function indexAction ()
    {
        $this->view->pageTitle = 'Outbound Messages';
        $this->assets->collection('css')->addCss('css/plugins/datatables/jquery.dataTables.css');

        $page  = $this->request->getQuery('page', 'int', 1);
        $count = $this->request->getQuery('cnt', 'int', $this->config->view->pagination);

        $query = $this->request->getQuery();

        $builder = $this->modelsManager->createBuilder()
            ->columns('*')
            ->addFrom('App\Model\Outbounds', 'outbound')
            ->orderBy('outbound.id DESC')
            ->where("1=1");

        $paginator = new Paginator([
            "builder" => $builder,
            "limit"   => $count,
            "page"    => $page
        ]);

        $this->view->paginator = $paginator->getPaginate();
    }
}