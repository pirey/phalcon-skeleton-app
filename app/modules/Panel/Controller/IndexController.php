<?php

namespace Panel\Controller;

use App\Mvc\Controller;

class IndexController extends Controller
{
    const TOTAL_DAILY_INB = 'stats.total.daily.inbounds';
    const TOTAL_DAILY_OUTB = 'stats.total.daily.outbounds';
    const TOTAL_INVALID_INB = 'stats.total.invalid.inbounds';

    public function indexAction()
    {
        $this->view->pageTitle = "Dashboard";

        $this->assets->collection('jsFooter')
            ->addJs('brio/js/plugins/DevExpressChartJS/dx.chartjs.js')
            ->addJs('brio/js/plugins/DevExpressChartJS/demo-charts.js');

        // Inbounds ----------------------------------------------------------------------------------------------------
        $stats = $this->cache->get(self::TOTAL_DAILY_INB);
        if ($stats === null) {
            $q = "SELECT COUNT(id) AS cnt, DATE(created_at) AS cat FROM inbounds WHERE DATEDIFF(NOW(), created_at) < 10 GROUP BY cat;";

            $statement = $this->db->prepare($q);
            $stat = $this->db->executePrepared($statement,[], null)->fetchAll();

            $o = [];
            foreach ($stat as $item) {
                $o[$item['cat']] = $item['cnt'];
            }
            $stats = $o;

            $this->cache->save(self::TOTAL_DAILY_INB, $stats, 60);
        }
        $this->view->dailyInbounds= $stats;

        // Outbounds ---------------------------------------------------------------------------------------------------
        $stats = $this->cache->get(self::TOTAL_DAILY_OUTB);
        if ($stats === null) {
            $q = "SELECT COUNT(id) AS cnt, DATE(created_at) AS cat FROM outbounds WHERE DATEDIFF(NOW(), created_at) < 10 GROUP BY cat;";

            $statement = $this->db->prepare($q);
            $stat = $this->db->executePrepared($statement,[], null)->fetchAll();
            $o = [];
            foreach ($stat as $item) {
                $o[$item['cat']] = $item['cnt'];
            }
            $stats = $o;
            $this->cache->save(self::TOTAL_DAILY_OUTB, $stats, 60);
        }
        $this->view->dailyOutbounds= $stats;

        // Invalids ----------------------------------------------------------------------------------------------------
        $stats = $this->cache->get(self::TOTAL_INVALID_INB);
        if ($stats === null) {
            $q = "SELECT COUNT(id) AS cnt, invalid FROM inbounds GROUP BY invalid;";
            $statement = $this->db->prepare($q);
            $stats = $this->db->executePrepared($statement,[], null)->fetchAll(2);
            $this->cache->save(self::TOTAL_INVALID_INB, $stats, 60);
        }
        $this->view->invalidsInbounds = $stats;
    }
}