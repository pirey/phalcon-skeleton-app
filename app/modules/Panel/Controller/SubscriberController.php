<?php

/**
 * @author Aboozar Ghafari <a6oozar@gmail.com>
 */

namespace Panel\Controller;
use Phalcon\Paginator\Adapter\QueryBuilder as Paginator;

use App\Mvc\Controller;
use App\Model\Subscribers;
use App\Model\SubscriptionLogs;

class SubscriberController extends Controller
{

    public function indexAction ()
    {
        $this->view->pageTitle = 'Subscribers';
        $this->assets->collection('css')
             ->addCss('brio/css/plugins/datatables/jquery.dataTables.css');

        $page  = $this->request->getQuery('page', 'int', 1);
        $count = $this->request->getQuery('cnt', 'int', $this->config->view->pagination);

        $builder = $this->modelsManager->createBuilder()
            ->columns('*')
            ->addFrom('App\Model\Subscribers', 'subs')
            ->orderBy('subs.id')
            ->where("1=1");

        $query = $this->request->getQuery();

        if (isset($query['msisdn']) && !empty($query['msisdn'])) {
            $builder->andWhere('msisdn = :MSISDN:', array('MSISDN' => $query['msisdn']));
        }
        if (isset($query['fk_services_id']) && !empty($query['fk_services_id'])) {
            $builder->andWhere('fk_services_id = :SERVICE_ID:', array('SERVICE_ID' => $query['fk_services_id']));
        }

        $paginator = new Paginator([
            "builder" => $builder,
            "limit"   => $count,
            "page"    => $page
        ]);

        $this->view->paginator = $paginator->getPaginate();
        $this->view->q = $query;
    }

    public function toggleAction ()
    {
        $id = $this->request->get('id', 'int');
        $page = $this->request->get('page', 'int', 1);
        $subscriber = Subscribers::findFirst($id);

        if (!$subscriber) {
            $this->flashSession->error('Requested Subscriber not found!');
            return $this->response->redirect('panel/subscriber');
        }

        $subscriber->flag = ($subscriber->flag == 0 ? 1 : 0);
        $subscriber->save();

        $subscriptionLog = new SubscriptionLogs();
        $subscriptionLog->msisn = $subscriber->msisdn;
        $subscriptionLog->command = 0;
        $subscriptionLog->by_whom = 1; // @todo operator ID from session
        $subscriptionLog->created_at = date('Y-m-d H:i:s');
        $subscriptionLog->save();

        $this->flashSession->notice('Subscription <b>'.($subscriber->flag == 0 ? 'Disabled' : 'Enabled').'</b> successfully.!');
        return $this->response->redirect('panel/subscriber?page='.$page);
    }
}