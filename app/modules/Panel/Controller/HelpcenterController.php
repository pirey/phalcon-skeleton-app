<?php

/**
 * @author Aboozar Ghafari <a6oozar@gmail.com>
 */

namespace Panel\Controller;

use App\Mvc\Controller;

class HelpcenterController extends Controller
{
    public function indexAction ()
    {
        $this->view->pageTitle = 'Helpcenter';
    }
}