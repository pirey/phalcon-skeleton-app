<?php

/**
 * @author Aboozar Ghafari <a6oozar@gmail.com>
 */

namespace Panel\Controller;
use Phalcon\Paginator\Adapter\NativeArray as ArrayPaginator;
use Phalcon\Paginator\Adapter\QueryBuilder as Paginator;

use App\Mvc\Controller;
use App\Model\Inbounds;

class InboundController extends Controller
{

//    public function indexAction()
//    {
//        $page  = $this->request->getQuery('page', 'int', 1);
//        $count = $this->request->getQuery('cnt', 'int', $this->config->view->pagination);
//
//
//        $paginator = new \Phalcon\Paginator\Adapter\Model(
//            array(
//                "data" => Inbounds::find(),
//                "limit"=> $count,
//                "page" => $page
//            )
//        );
//
//        $paginator = $paginator->getPaginate();
//        $this->view->paginator = $paginator;
//    }


    public function indexAction ()
    {
        $this->view->pageTitle = 'Inbound Messages';
        $this->assets->collection('css')->addCss('css/plugins/datatables/jquery.dataTables.css');

        $page  = $this->request->getQuery('page', 'int', 1);
        $count = $this->request->getQuery('cnt', 'int', $this->config->view->pagination);

        $query = $this->request->getQuery();

        $builder = $this->modelsManager->createBuilder()
            ->columns('*')
            ->addFrom('App\Model\Inbounds', 'inbound')
            ->orderBy('inbound.id DESC')
            ->where("1=1");

//        if (isset($query['filename']) && strlen(trim($query['filename'])) > 0) {
//            $builder->andWhere('file_name LIKE "%'. $query['filename'].'%"');
//        }
//
//        if (isset($query['uuid']) && strlen(trim($query['uuid'])) > 0) {
//            $builder->andWhere('uuid = "'. $query['uuid'].'"');
//        }
//
//        if (isset($query['file_status']) && is_numeric($query['file_status'])) {
//            $builder->andWhere('file_status = ' . $query['file_status']);
//        }
//
        $paginator = new Paginator([
            "builder" => $builder,
            "limit"   => $count,
            "page"    => $page
        ]);

        $this->view->paginator = $paginator->getPaginate();
//        $this->view->subscriber = $subscriber;
//
//
//        $filterForm = new FilesFilter();
//        $filterForm->setDefaults($query);
//        $this->view->filterForm = $filterForm;
    }
}