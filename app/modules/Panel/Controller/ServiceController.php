<?php

/**
 * @author Aboozar Ghafari <a6oozar@gmail.com>
 */

namespace Panel\Controller;
use App\Model\Keywords;
use Phalcon\Paginator\Adapter\QueryBuilder as Paginator;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

use App\Mvc\Controller;
use App\Model\Services;

class ServiceController extends Controller
{

    public function indexAction ()
    {
        $this->view->pageTitle = 'Outbound Messages';
        $this->assets->collection('css')
            ->addCss('brio/css/plugins/datatables/jquery.dataTables.css');

        $page  = $this->request->getQuery('page', 'int', 1);
        $count = $this->request->getQuery('cnt', 'int', $this->config->view->pagination);

        $query = $this->request->getQuery();

        $builder = $this->modelsManager->createBuilder()
            ->columns('*')
            ->addFrom('App\Model\Services', 'serv')
            ->orderBy('serv.name')
            ->where("1=1");

        $paginator = new Paginator([
            "builder" => $builder,
            "limit"   => $count,
            "page"    => $page
        ]);

        $this->view->paginator = $paginator->getPaginate();
    }

    public function readAction()
    {
        $this->assets->collection('css')->addCss('brio/css/app/email.css');
        $serviceId = $this->request->get('id', 'int');
        $service = Services::findFirst($serviceId);

        if (!$service) {
            $this->flashSession->warning(sprintf("Service with id `%d` not found!", $serviceId));
            return $this->dispatcher->forward(array("action" => "index"));
        }

        $this->view->service = $service;
        $this->view->pageTitle = $service->name;
    }

    public function createAction()
    {
        $this->assets->collection('css')
            ->addCss('brio/css/plugins/bootstrap-chosen/chosen.css')
            ->addCss('brio/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')
            ->addCss('brio/css/switch-buttons/switch-buttons.css');
        $this->assets->collection('jsFooter')
            ->addCss('brio/js/plugins/bootstrap-chosen/chosen.jquery.js')
            ->addCss('brio/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js');

        $form = new \Panel\Forms\ServiceForm();
        $service = new Services();

        $this->view->pageTitle = 'Create new service';

        if ($this->request->isPost())
        {
            $post = $this->request->getPost();
            if ($form->isValid($post) != false)
            {
                $keywords = explode(',', $post['keywords']);
                if (empty($post['dedicated']) && !count($keywords)) {
                    $this->flashSession->error('Keyword required for non dedicated service.');
                } else {

                    try {

                        $manager = new TransactionManager();

                        $transaction = $manager->get();

                        if ($service->save($post) == false) {
                            $transaction->rollback($service->getMessages()[0]);
                        }

                        if ($service->keywords->count()) {
                            foreach ($service->keywords as $keyword) {
                                if ($keyword->delete() == false) {
                                    $transaction->rollback(sprintf("Can't delete %s keyword", $keyword->txt));
                                }
                            }
                        }

                        foreach ($keywords as $keyword) {
                            $keywordModel = new Keywords();
                            $keywordModel->fk_services_id = $service->id;
                            $keywordModel->txt = $keyword;
                            if ($keywordModel->save() == false) {
                                $transaction->rollback($keywordModel->getMessages()[0]);
                            }
                        }

                        $transaction->commit();
                        $this->flashSession->success('The Service has been added successfully.');
                        return $this->response->redirect('panel/service');

                    } catch(\Phalcon\Mvc\Model\Transaction\Failed $e) {
                        $this->flashSession->error($e->getMessage());
                    }
                }
            } else {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            }
        }

        $this->view->form = $form;
    }

    public function updateAction()
    {
        $this->assets->collection('css')
            ->addCss('brio/css/plugins/bootstrap-chosen/chosen.css')
            ->addCss('brio/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')
            ->addCss('brio/css/switch-buttons/switch-buttons.css');
        $this->assets->collection('jsFooter')
            ->addCss('brio/js/plugins/bootstrap-chosen/chosen.jquery.js')
            ->addCss('brio/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js');


        $id = $this->request->get('id', 'int');
        $service = Services::findFirst($id);

        if (!$service) {
            $this->flashSession->error('Requested Service not found');

            return $this->response->redirect('panel/service');
        }
        $form = new \Panel\Forms\ServiceForm($service);

        $this->view->pageTitle = 'Update '. $service->name;

        if ($this->request->isPost())
        {
            $post = $this->request->getPost();
            if ($form->isValid($post) != false)
            {
                $keywords = explode(',', $post['keywords']);
                if (empty($post['dedicated']) && !count($keywords)) {
                    $this->flashSession->error('Keyword required for non dedicated service.');
                } else {

                    try {

                        $manager = new TransactionManager();

                        $transaction = $manager->get();

                        if ($service->save($post) == false) {
                            $transaction->rollback($service->getMessages()[0]);
                        }

                        if ($service->keywords->count()) {
                            foreach ($service->keywords as $keyword) {
                                if ($keyword->delete() == false) {
                                    $transaction->rollback(sprintf("Can't delete %s keyword", $keyword->txt));
                                }
                            }
                        }

                        foreach ($keywords as $keyword) {
                            $keywordModel                 = new Keywords();
                            $keywordModel->fk_services_id = $service->id;
                            $keywordModel->txt            = $keyword;
                            if ($keywordModel->save() == false) {
                                $transaction->rollback($keywordModel->getMessages()[0]);
                            }
                        }

                        $transaction->commit();
                        $this->flashSession->success('The Service has been added successfully.');

                        return $this->response->redirect('panel/service');

                    } catch (\Phalcon\Mvc\Model\Transaction\Failed $e) {
                        $this->flashSession->error($e->getMessage());
                    }
                }
            } else {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            }
        } else {
            $keywords = $service->getKeywords();
            if ($keywords->count()) {
                $serviceKeywords = [];
                foreach ($keywords as $keyword) {
                    $serviceKeywords [] = $keyword->txt;
                }
            }
            $form->getElements()['keywords']->setDefault(implode(',', $serviceKeywords));
        }

        $this->view->form = $form;
    }

    public function deleteAction()
    {
        $id = $this->request->get('id', 'int');
        $service = Services::findFirst($id);

        if (!$service) {
            $this->flashSession->error('Requested Service not found');

            return $this->response->redirect('panel/service');
        }

        if ($service->subscribers->count()) {
            $this->flashSession->error('Service has subscribers and could not be delete.');

            return $this->response->redirect('panel/service');
        }

        $service->delete();
        $this->flashSession->success('Service deleted successfully.');
        return $this->response->redirect('panel/service');
    }
}