<?php

/**
 * @author Aboozar Ghafari <a6oozar@gmail.com>
 */

namespace Panel;

use Phalcon\Mvc\ModuleDefinitionInterface;

class Module implements ModuleDefinitionInterface
{

    public function registerAutoloaders()
    {
        // Registers an autoloader related to the module
    }

    public function registerServices($di)
    {
        $dispatcher = $di->get('dispatcher');
        $dispatcher->setDefaultNamespace('Panel\Controller');
        $di->set('dispatcher', $dispatcher);

        $view = $di->get('view');
        $view->setViewsDir(__DIR__ . '/views/');
        $view->setPartialsDir('../../../views/partials/');
        $di->set('view', $view);

    }

}