<?php
namespace Panel\Forms;

use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\Regex as RegexValidator;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\PresenceOf;
use Tartan\Forms\Element\Select;
use App\Model\Services;

class ServiceForm extends \App\Forms\Form
{
    public function initialize ($entity = null, $options = null)
    {
        //name
        $el = new Text('name', [
            'required'    => true,
            'placeholder' => self::_('Service Name'),
            'class'       => 'form-control form-control-flat rtl',
            'autofocus'   => 'autofocus'
        ]);
        $el->setLabel(self::_('Name'));

        $this->add($el);

        //mciServiceId
        $el = new Text('mci_service_id', [
            'required' => true,
            'class'    => 'form-control ltr',
        ]);
        $el->setLabel('MCI Service ID');

        $el->addValidators(array(
            new RegexValidator(array(
                'pattern' => '/^[0-9]{1,}$/',
                'message' => 'The MCI service ID is invalid'
            ))
        ));

        $this->add($el);

        $el = new Check("status", [
            'class'      => 'form-control',
            'value'      => 1,
            'emptyValue' => 0
        ]);
        $this->add($el);

        //odURL
        $el = new Text('od_url', [
            'required'    => true,
            'class'       => 'form-control ltr',
            'placeholder' => 'http://localhost/app.php?inbound_id={inbound_id}&msisdn={msisdn}&content={content}&service_id={service_id}&time={time}'
        ]);
        $el->setLabel('OD URL');
        $this->add($el);

        //odURL
        $el = new Text('ac_url', [
            'required'    => true,
            'class'       => 'form-control ltr',
            'placeholder' => 'http://localhost/app.php?inbound_id={inbound_id}&msisdn={msisdn}&content={content}&service_id={service_id}&time={time}'
        ]);
        $el->setLabel('AC URL');
        $this->add($el);

        // Categiry
        $el = new Select("cat", Services::Categories(), [
            'class' => 'form-control',
        ]);
        $this->add($el);

        $el = new Text('free_shortcode', [
            'class'       => 'form-control ltr',
            'placeholder' => self::_('Free Shortcode'),
        ]);
        $el->setLabel('Free Shortcode');

        $el->addValidators(array(
            new RegexValidator(array(
                'pattern' => '/^[0-9]{4,}$/',
                'message' => 'The Free Shortcode is invalid'
            ))
        ));
        $this->add($el);

        $el = new Text('cost_shortcode', [
            'class'       => 'form-control ltr',
            'placeholder' => self::_('Cost Shortcode'),
        ]);
        $el->setLabel('Free Shortcode');
        $this->add($el);

        $el = new Text('dedicated', [
            'class'       => 'form-control ltr',
            'placeholder' => self::_('Dedicated Shortcode'),
        ]);
        $el->setLabel('Dedicated Shortcode');
        $this->add($el);

        $el = new Text('slogan', [
            'class'     => 'form-control rtl',
            'autofocus' => 'autofocus',
            'maxlength' => 60
        ]);
        $el->setLabel('Slogan');

        $this->add($el);

        //keywords
        $el = new Text('keywords', [
            'class'       => 'form-control tagsinput'
        ]);
        $el->setLabel(self::_('Keywords'));
        $this->add($el);

        //CSRF
        $el = new Hidden('csrf');
        $el->addValidator(
            new Identical(array(
                'value'   => $this->security->getSessionToken(),
                'message' => 'CSRF validation failed'
            ))
        );
        $this->add($el);
    }
}