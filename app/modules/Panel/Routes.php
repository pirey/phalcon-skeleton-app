<?php

/**
 * Routes
 * @author Aboozar Ghafari <a6oozar@gmail.com>
 */

namespace Panel;

class Routes
{

    public function add($router)
    {
        // Homepage router
        $router->add('/dashboard', array(
            'module'     => 'panel',
            'controller' => 'index',
            'action'     => 'index',
        ))->setName('dashboard');

        $router->add("/panel", array(
            'module'     => 'panel',
            'controller' => 'index',
            'action'     => 'index',
        ));

        $router->add('/panel/:controller', array(
            'module'     => 'panel',
            'controller' => 1,
            'action'     => 'index',
        ));

        $router->add('/panel/:controller/:action', array(
            'module'     => 'panel',
            'controller' => 1,
            'action'     => 2,
        ));
        $router->add(
            "/panel/:controller/:action/:params",
            array(
                'module'     => 'panel',
                "controller" => 1,
                "action"     => 2,
                "params"     => 3,
            )
        );

        return $router;
    }

}