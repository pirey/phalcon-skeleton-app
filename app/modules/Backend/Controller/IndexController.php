<?php

/**
 * @author Aboozar Ghafari <a6oozar@gmail.com>
 */

namespace Backend\Controller;

use Phalcon\Mvc\Controller;
use Backend\Entity\Post;

class IndexController extends Controller
{

    public function indexAction()
    {
        exit('backend::index::index');
    }
}