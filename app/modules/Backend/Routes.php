<?php

/**
 * Routes
 * @author Aboozar Ghafari <a6oozar@gmail.com>
 */

namespace Backend;

class Routes
{

    public function add($router)
    {

        $backend = new \Phalcon\Mvc\Router\Group(array(
            'module'     => 'backend',
            'controller' => 'index',
            'action'     => 'index'
        ));
        $backend->setPrefix('/backend');

        $router->add('/backend(/:controller(/:action(/:params)?)?)?', array(
            'module'     => 'backend',
            'controller' => 2,
            'action'     => 4,
            'params'     => 6,
        ))->setName('backend');

        $backend->add('/', array(
            'action' => 'index',
        ))->setName('backendhome');

//        $backend->add('/{slug:[a-z0-9_-]+}.html', array(
//            'action' => 'post',
//        ))->setName('backend/post');

        $router->mount($backend);

        return $router;

    }

}