<?php

/**
 * @author Aboozar Ghafari <a6oozar@gmail.com>
 */

namespace Gateway;

use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Loader;

class Module implements ModuleDefinitionInterface
{

    public function registerAutoloaders()
    {
    }

    public function registerServices($di)
    {
        $dispatcher = $di->get('dispatcher');
        $dispatcher->setDefaultNamespace('Gateway\Controller');
        $di->set('dispatcher', $dispatcher);

        $view = $di->get('view');
        $view->disable();
        $di->set('view', $view);
    }

}
