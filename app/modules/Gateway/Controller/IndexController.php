<?php
namespace Gateway\Controller;

use Phalcon\Mvc\Controller;
use Tartan\Helper\Persian;

use App\Model\Inbounds;
use App\Model\Keywords;
use App\Model\Services;
use App\Model\Dictionary;
use App\Entity\ErrorCodes as Err;

class IndexController extends Controller
{
    public function receiveAction ()
    {
        try
        {
            $msisdn     = $this->request->getQuery('MSISDN', ['trim', 'alphanum']);
            $shortCode  = $this->request->getQuery('ShortCode', ['trim', 'int']);
            $rawMessage = strtolower($this->request->getQuery('Content', ['trim', 'string']));
            $time       = $this->request->getQuery('Time', ['trim', 'string']);
            $logger     = $this->logger;

            // check for invalid request
            if (empty($shortCode) || empty($msisdn)) {
                $logger->info('MSISDN/Shortcode is empty!');
                exit('error');
            }

            // Normalize spaces
            $message = preg_replace('!\s+!', ' ', Persian::normalizeFa(trim($rawMessage)));
            // Filter acceptable characters
            $message = preg_replace("/[^\x{0627}-\x{0628}\x{062A}-\x{0648}\x{06F0}-\x{06F9} \x{0622}\x{06A9}\x{06AF}\x{067E}\x{0698}\x{06CC}\x{0686}\x{060C} A-Za-z0-9 !?#$%^&*().,:;=@~`'\"]/u", '', $message);

            $tmpMessage = $message;

            // CHECK FOR DEDICATED SHORTCODE ---------------------------------------------------------------------------
            $dedicatedShortCodeKey = 'service.by.dedicated.' . $shortCode;

            $service = $this->cache->get($dedicatedShortCodeKey);
            if ($service == null) {
                $service = Services::findFirst([
                    "conditions" => "dedicated = ?1",
                    "bind"       => [1 => $shortCode]
                ]);

                $this->cache->save($dedicatedShortCodeKey, $service, 86400); // 1 day
            }

            // dedicated shortcode detected ----------------------------------------------------------------------------
            if ($service instanceof Services) {
                $this->_debug(sprintf('Detected DEDICATED shortcode service >> %s', $service->name));
                $inbounds                 = new Inbounds();
                $inbounds->msisdn         = $msisdn;
                $inbounds->shortcode      = $shortCode;
                $inbounds->raw_content    = $rawMessage;
                $inbounds->content        = $message;
                $inbounds->shamsi_date    = $time;
                $inbounds->autocharge     = 0;
                $inbounds->fk_services_id = $service->id;
                $inbounds->created_at     = date('Y-m-d H:i:s');
                if (!$inbounds->save()) {
                    print_r($inbounds->getMessages());
                    // log the error
                    $log = $logger->emergency('Could not save inbound!',
                        'inbound', 0, $service->id, $inbounds->getMessages()
                    );
                    // notify development
                    $this->notify->add(
                        $this->config->notification->developerId,
                        'Could not save inbound!',
                        'development',
                        'inbound',
                        '/error/' . $log->id
                    );
                }
                exit('done.');
            }
            // END CHECK FOR DEDICATED SHORTCODE -----------------------------------------------------------------------

            // NOTHING dedicated detected ------------------------------------------------------------------------------
            if (empty($rawMessage))
            {
                $inbounds                 = new Inbounds();
                $inbounds->msisdn         = $msisdn;
                $inbounds->shortcode      = $shortCode;
                $inbounds->raw_content    = '';
                $inbounds->content        = '';
                $inbounds->created_at     = date('Y-m-d H:i:s');
                $inbounds->shamsi_date    = $time;
                $inbounds->autocharge     = 0;
                $inbounds->invalid        = Err::EMPTY_MESSAGE_CONTENT;
                $inbounds->fk_services_id = 0;
                $inbounds->created_at     = date('Y-m-d H:i:s');
                $inbounds->save();
                exit('done.');
            }

            // extract keyword from content
            if (preg_match('/(خاموش|off)$/i', $message)) {
                $keyword = trim(preg_replace('/(خاموش|off)/i', '', $message));
            }
            else {
                $keyword = explode(' ', trim($message))[0];
            }

            // remove keyword from content
            $message = trim(preg_replace("/$keyword/i", '', $message));

            $this->_debug(sprintf('Detected keyword >> %s', $keyword));

            $inbounds              = new Inbounds();
            $inbounds->msisdn      = $msisdn;
            $inbounds->shortcode   = $shortCode;
            $inbounds->raw_content = $rawMessage;
            $inbounds->content     = empty($message) ? $tmpMessage : $message;
            $inbounds->shamsi_date = $time;
            $inbounds->autocharge  = 0;

            $serviceKey = 'service.by.keyword.' . $keyword;

            $service = $this->cache->get($serviceKey);

            if ($service === null) {
                // service does not exists in cache

                // trying to detect keyword
                $KeywordRow = Keywords::findFirst([
                    "conditions" => "txt = ?1",
                    "bind"       => [1 => $keyword]
                ]);

                // keyword not found
                if (!$KeywordRow) {
                    $inbounds->content = $tmpMessage;
                    $inbounds->invalid = Err::KEYWORD_NOT_FOUND;
                    $r = $inbounds->save();
                    $inboundID = $r ? $inbounds->id : 0;

                    $logger->info('inbound', $inboundID, 0, 'Keyword ('.$keyword.') Not found!');

                    $this->_debug('Keyword Not found!');
                    exit('done.');
                }

                // keyword found
                $service = $KeywordRow->service;

                // keyword found
                if ($service) {
                    // service exists
                    $this->cache->save($serviceKey, $service, 86400); // 1 day
                }
                else {
                    // service not exists
                    $inbounds->invalid        = Err::SERVICE_NOT_FOUND;
                    $inbounds->fk_services_id = $KeywordRow->fk_services_id;
                    $inbounds->save();

                    $err = sprintf('Service %d not exists!', $KeywordRow->fk_services_id);
                    $logger->emergency($err);

                    $this->_debug($err);
                    exit('done.');
                }
            } // caching done

            // SERVICE found
            $this->_debug(sprintf('Detected Service >> %s', $service->name));


            // check (invalid) shortcode for detected service
            if ($shortCode != $service->free_shortcode && $shortCode != $service->cost_shortcode)
            {
                $inbounds->invalid = Err::INVALID_SHORTCODE;
                $r = $inbounds->save();
                $inboundID = $r ? $inbounds->id : 0;

                $logger->info('Invalid shortcode!', 'inbound', $inboundID, $inbounds->fk_services_id);

                $this->_debug('Invalid shortcode!');
                exit('done.');
            }

            // check for service status
            if (!$service->status)
            {
                $inbounds->invalid = Err::SERVICE_UNAVAILABLE;
                $inbounds->save();

                $r = $inbounds->save();
                $inboundID = $r ? $inbounds->id : 0;

                $logger->info('Service unavailable!' , 'inbound', $inboundID, $inbounds->fk_services_id);

                $this->_debug('Service unavailable!');
                exit('done.');
            }

            // inbound handled
            $inbounds->fk_services_id = $service->id;
            $inbounds->save();
            print_r($inbounds->getMessages());
            exit('done.');

        } catch (\Exception $e) {
            $this->logger->emergency($e->getMessage(), 'inbound', 0, 0, $e);
            throw $e;
        }
    }

    public function deliveryAction ()
    {
        $model = new \App\Model\AppModel();
        $db = $model->getWriteConnection();

        $refId = $this->request->getQuery('RefId', ['int']);
        $db->execute("UPDATE outbounds SET processed = 2 WHERE success_token = ?", [$refId]);
        exit('done');
    }

    public function invalidAction()
    {
        $shortcode  = $this->request->getQuery('shortcode', ['int']);
        $code       = $this->request->getQuery('code', ['int']);

        switch ($code)
        {
            case 105:
                $message = Dictionary::EMPTY_MESSAGE;
                break;
            default:
                $message = Dictionary::INVALID_MESSAGE;
                break;

        }


        $slogans = unserialize($this->redis->get('slogans'));
        if (is_array($slogans)) {
            foreach ($slogans as $slogan) {
                if (mb_strlen(utf8_decode($message) . PHP_EOL . $slogan) < 350) {
                    $message = $message . PHP_EOL . $slogan;
                }
            }
        }

        $message = strtr($message, [
            '{domain}'   => $this->config->company->domain,
            '{company}'  => $this->config->company->name,
            '{tel}'      => $this->config->company->tel,
            '{dfshcode}' => $this->config->application->defaultFreeShortcode,
        ]);

        header('Content-Type: application/json');

        $response = [
            'content'   => $message,
            'shortcode' => $this->config->application->defaultFreeShortcode
        ];

        exit(json_encode($response));
    }

    private function  _debug ($message)
    {
        echo $message . PHP_EOL;
    }
}