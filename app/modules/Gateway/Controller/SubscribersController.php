<?php
namespace Gateway\Controller;

use Phalcon\Mvc\Controller;
use App\Model\Subscribers;

class SubscribersController extends Controller
{
    public function initialize ()
    {
        $this->view->disable();
    }

    public function indexAction ()
    {
        exit('Nothing to see here!');
    }

    public function UnsubscribeAction ()
    {
        try {
            $msisdn     = $this->request->getQuery('msisdn', ['trim', 'alphanum']);
            $serviceId  = $this->request->getQuery('service_id', ['trim', 'int']);

            // check for invalid request
            if (empty($msisdn) || empty($serviceId)) {
                $this->logger->info('MSISDN/Service Id is empty!');
                $this->output('msisdn or service_id is empty!', 1);
            }

            $model = new \App\Model\AppModel();
            $db = $model->getWriteConnection();

            $db->execute("INSERT INTO subscription_logs (msisdn, command) VALUE (?, 0)", [$msisdn]);
            $db->execute("UPDATE subscribers SET flag = 0 WHERE fk_service_id = ? AND msisdn = ?", [$serviceId, $msisdn]);

            $this->output('done');

        } catch (\Exception $e) {
            $this->output($e->getMessage(), 1);
        }
    }

    public function SubscribeAction ()
    {
        try {
            $msisdn     = $this->request->getQuery('msisdn', ['trim', 'alphanum']);
            $serviceId  = $this->request->getQuery('service_id', ['trim', 'int']);

            // check for invalid request
            if (empty($msisdn) || empty($serviceId)) {
                $this->logger->info('MSISDN/Service Id is empty!');
                $this->output('msisdn or service_id is empty!', 1);
            }

            $model = new \App\Model\AppModel();
            $db = $model->getWriteConnection();

            $db->execute("INSERT INTO subscription_logs (msisdn, command) VALUE (?, 1)", [$msisdn]);
            $db->execute("INSERT INTO subscribers (msisdn, fk_service_id) VALUE (?, ?) ON DUPLICATE KEY UPDATE flag = 1", [$msisdn, $serviceId]);

            $this->output('done');

        } catch (\Exception $e) {
            $this->output($e->getMessage(), 1);
        }
    }

    protected function output($message, $error = 0)
    {
        header('Content-Type: application/json');
        $r = [
            'message' => $message,
            'error'   => $error
        ];
        echo json_encode($r);
        exit;
    }
}