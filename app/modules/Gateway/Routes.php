<?php
namespace Gateway;

class Routes
{

    public function add ($router)
    {
        $router->add("/gateway", array(
            'module'     => 'gateway',
            'controller' => 'index',
            'action'     => 'index',
        ));

        $router->add('/gateway/:controller', array(
            'module'     => 'gateway',
            'controller' => 1,
            'action'     => 'index',
        ));

        $router->add('/gateway/:controller/:action', array(
            'module'     => 'gateway',
            'controller' => 1,
            'action'     => 2,
        ));
        $router->add(
            "/gateway/:controller/:action/:params",
            array(
                'module'     => 'gateway',
                "controller" => 1,
                "action"     => 2,
                "params"     => 3,
            )
        );

        $router->add('/receive', [
            'module'     => 'gateway',
            'controller' => 'index',
            'action'     => 'receive',
        ])->setName('receive-msg');

        $router->add('/delivery', [
            'module'     => 'gateway',
            'controller' => 'index',
            'action'     => 'delivery',
        ])->setName('delivery-msg');

        $router->add('/remain', [
            'module'     => 'gateway',
            'controller' => 'index',
            'action'     => 'remain',
        ])->setName('remain-msg');

        $router->add('/invalid', [
            'module'     => 'gateway',
            'controller' => 'index',
            'action'     => 'invalid',
        ])->setName('invalid-msg');

        $router->add('/subscribe', [
            'module'     => 'gateway',
            'controller' => 'subscribers',
            'action'     => 'subscribe',
        ])->setName('subscribe-service');

        $router->add('/unsubscribe', [
            'module'     => 'gateway',
            'controller' => 'subscribers',
            'action'     => 'unsubscribe',
        ])->setName('unsubscribe-service');

        return $router;
    }
}