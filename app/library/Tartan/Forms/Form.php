<?php
namespace Tartan\Forms;
use Phalcon\Forms\Form as PhalconForm;

class Form extends PhalconForm
{
    public function renderFormErrors()
    {
        if (count($this->getMessages()) > 0) {
            $html = '';
            foreach ($this->getMessages() as $message) {
                $html .= "<li>{$message}</li>";
            }

            return strtr('<ul>{items}</ul>', ['{items}' => $html]);

        } else {
            return '';
        }
    }
}