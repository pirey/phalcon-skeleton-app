<?php
/**
 * @package Tartan Framework (extended PhalconPhp Framework)
 * @namespace Tartan\Forms\Element
 * @class Tartan\Forms\Element\Captcha
 * @author  Tartan <me[@]tartan[.]pro>
 * @link http://git.tartan.pro
 * @license No license. Completely free, enjoy ;)
 */

namespace Tartan\Forms\Element
{
    use Phalcon\DI;
    use Phalcon\Forms\Element\Text;
    class Captcha extends Text
    {
        /**
         * Renders the element widget
         *
         * @param array $attributes
         * @return string
         */
        public function render($attributes=null)
        {
            $elementId = $this->getAttribute('id') ? $this->getAttribute('id') : $this->getName();
            $this->setAttribute('style', 'direction:ltr');
            $imageId = $elementId.'-img-'.uniqid();

$captchaHtml = <<< EOD
<div class="captcha-wrapper {wrapper-class}">
    <img src="{url}" id="{imageId}" class="captcha-image {image-class}" /><br/>
    <a href="#"
    style="line-height:26px"
    onclick="
        document.getElementById('{imageId}').src='{url}?'+Math.random();
        document.getElementById('{inputId}').focus(); return false;"
        id="{change-captcha}" class="captcha-change">
        <i class="fa fa-refresh"></i> {refreshMessage}
    </a>
</div>
EOD;
            $captchaHtml = strtr($captchaHtml, array(
                '{imageId}'       => $imageId,
                '{inputId}'       => $elementId,
                '{url}'           => DI::getDefault()->get('url')->get($this->getUserOption('url', 'site/captcha/index/')),
                '{wrapper-class}' => $this->getUserOption('wrapper-class', ''),
                '{image-class}'   => $this->getUserOption('image-class', ''),
                '{change-captcha}'=> $imageId.'-change',
                '{refreshMessage}'=> $this->getUserOption('refresh-message', 'Not readable? Change text.'),
            ));

            $this->clear();
            return parent::render($attributes) . $captchaHtml . '';
        }
    }
}
