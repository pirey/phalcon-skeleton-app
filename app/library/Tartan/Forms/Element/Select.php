<?php
namespace Tartan\Forms\Element {
    class Select extends \Phalcon\Forms\Element\Select
    {
        public function render ($attributes = null)
        {

            $thisValue = is_null($this->getValue()) ? '' : $this->getValue();

            if (is_array($attributes)) {
                $attributes = array_merge($this->getAttributes(), $attributes);
            }
            else {
                $attributes = $this->getAttributes();
            }
            if (empty($attributes)) {
                $attributes = [];
            }

            $emptyOption = '';
            if (isset($attributes['useEmpty'])) {
                if (isset($attributes['emptyValue'])) {
                    $emptyValue = $attributes['emptyValue'];
                }
                else {
                    $emptyValue = '';
                }

                $emptyText = isset($attributes['emptyText']) ? $attributes['emptyText'] : 'Choose...';

                $selected = '';

                if ($thisValue === $emptyValue) {
                    $selected = 'selected="selected"';
                }
                $emptyOption = '<option ' . $selected . ' value="' . $emptyValue . '">' . $emptyText . '</option>';
            }

            $html = '<select id="{ITEM_ID}" name="{ITEM_NAME}" {ITEM_ATTRIBUTES}">{EMPTY_OPTIONS}{OPTIONS}</select>';

            $options = '';

            foreach ($this->getOptions() as $key => $value) {
                if (isset($attributes['using'])) 
                {
                    $key   = $value->{$attributes['using'][0]};
                    $value = $value->{$attributes['using'][1]};
                }
                $options .= '<option ' . ($thisValue === strval($key) ? 'selected="selected"' : '') . ' value="' . $key . '">' . $value . '</option>' . PHP_EOL;
            }

            unset($attributes['useEmpty'], $attributes['emptyText'], $attributes['emptyValue'], $attributes['using']);

            $attrs = '';
            if (is_array($attributes)) {
                foreach ($attributes as $key => $value) {
                    $attrs .= "{$key}='{$value}'";
                }
            }

            return strtr($html, [
                '{ITEM_ID}'         => $this->getAttribute('id') ? $this->getAttribute('id') : $this->getName(),
                '{ITEM_NAME}'       => $this->getName(),
                '{EMPTY_OPTIONS}'   => $emptyOption,
                '{OPTIONS}'         => $options,
                '{ITEM_ATTRIBUTES}' => $attrs
            ]);
        }
    }
}