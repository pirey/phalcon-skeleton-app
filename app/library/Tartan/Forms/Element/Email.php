<?php
/**
 * @package Tartan Framework (extended PhalconPhp Framework)
 * @namespace Tartan\Forms\Element
 * @class Tartan\Forms\Element\Captcha
 * @author  Tartan me[@]tartan[.]pro
 * @link http://git.tartan.pro
 * @license No license. Completely free, enjoy ;)
 */

namespace Tartan\Forms\Element
{
    use Phalcon\Forms\Element\Text;
    class Email extends Text
    {
        public function render($attributes=null)
        {
            $elementId = $this->getAttribute('id') ? $this->getAttribute('id') : $this->getName();

            // forced ltr direction
            $this->setAttribute('class', $this->getAttribute('class') . ' ltr');
            $this->setAttribute('style', 'direction:ltr');

//            $this->clear();
            $html = parent::render($attributes);

            $html = str_replace('type="text"', 'type="email"', $html);
            return $html;
        }
    }
}