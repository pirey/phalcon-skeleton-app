Tartan\Breadcrumb - Breadcrumbs for PhalconPHP applications
===

This is a powerful and extremely easy to use breadcrumb add-on with optional multi-language support.

I recommend registering it with your application's services for even easier use

```php
$di->set('crumbs', function() {
    return new \Tartan\Breadcrumb\Breadcrumbs();
});
```

#How to use this add-on
###Adding a crumb with a link
```php
$this->crumbs->add('home', '/', 'Home');
```

###Adding a crumb without a link (Normally the last one)
```php
$this->crumbs->add('user', null, 'User', false);
```

###Render crumbs
```php
$this->crumbs->render();
```

###Update an existing crumb
```php
$params = ['label' => 'Account'];
$this->crumbs->update('user', $params);
```

###Delete a crumb
```php
$this->crumbs->remove('user');
```

###Change crumb separator
```php
$this->crumbs->setSeparator(' &raquo; ');
```

###Add multi-language support
```php
$messages = [
    'crumb-home' => 'Home',
    'crumb-user' => 'User',
    'crumb-settings' => 'Settings',
    'crumb-profile' => 'Profile'
];
$translate = new \Phalcon\Translate\Adapter\NativeArray([
    'content'   => $messages
]);
$this->crumbs->useTranslation($translate);



$this->crumbs->setSeparator(' &raquo; ')
                ->add('home', '/', 'crumb-home')
                ->add('user', '/user', 'crumb-user')
                ->add('profile', '/user/profile', 'crumb-profile')
                ->add('settings', '', 'crumb-settings', false);
$this->crumbs->render();
```