<?php
namespace Tartan\Flash;

class Session extends \Phalcon\Flash\Session
{
    public function output($remove=null)
    {
        $messages = $this->getMessages();
        if (!count($messages)) {
            return;
        }
        $html = '';
        foreach ($messages as $type => $msgs)
        {
            $class = isset($this->_cssClasses [$type]) ? $this->_cssClasses [$type] : $type;
            $html .= '
            <div class="'.$class.' alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                </button>'.
                implode('<br/>', $msgs) .
            '</div>';
        }
        echo $html;
    }
}