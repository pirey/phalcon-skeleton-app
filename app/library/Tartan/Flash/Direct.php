<?php
namespace Tartan\Flash;

class Direct extends \Phalcon\Flash\Direct
{
    public function outputMessage($type, $message)
    {
        $html = '';
        $class = isset($this->_cssClasses [$type]) ? $this->_cssClasses [$type] : $type;
        $html .= '
        <div class="'.$class.' alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">
                <span aria-hidden="true">×</span><span class="sr-only">Close</span>
            </button>'.
            $message .
        '</div>';
        echo $html;
    }
}