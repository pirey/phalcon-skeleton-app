<?php

namespace Tartan\Auth\Model;

use Tartan\Model;

/**
 * SuccessLogins
 *
 * This model registers successful logins registered users have made
 */
class SuccessLogins extends Model
{
    public $id;
    public $fk_member_id;
    public $ip_address;
    public $user_agent;

    public function initialize ()
    {
        parent::initialize();
        $this->setSource("success_logins");

        $this->belongsTo('fk_member_id', 'Tartan\Auth\Model\Members', 'id', array(
            'alias' => 'member'
        ));
    }

}