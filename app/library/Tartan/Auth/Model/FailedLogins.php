<?php

namespace Tartan\Auth\Model;

use Tartan\Model;

/**
 * FailedLogins
 *
 * This model registers unsuccessful logins registered and non-registered users have made
 */
class FailedLogins extends Model
{
    public $id;
    public $fk_member_id;
    public $ip_address;
    public $attempted;

    public function initialize ()
    {
        parent::initialize();
        $this->setSource("members");

        $this->belongsTo('fk_member_id', 'Tartan\Auth\Model\Members', 'id', array(
            'alias' => 'member'
        ));
    }

}