<?php

/**
 * News
 * @copyright Copyright (c) 2011 - 2012 Aleksandr Torosh (http://wezoom.com.ua)
 * @author    Aboozar Ghafari <a6oozar@gmail.com>
 */

namespace Tartan\Auth\Model;

use Phalcon\DI;
use Phalcon\Mvc\Model\Behavior\Timestampable;
use Phalcon\Mvc\Model\Validator\Email;
use Phalcon\Mvc\Model\Validator\InclusionIn;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use Tartan\Model;

class Members extends Model
{
    const ST_REGISTERED = 'normal';
    const ST_RESEND     = 'resend';

    public $id;
    public $identity;
    public $name;
    public $password;
    public $salt;
    public $role;
    public $status;
    public $is_active;
    public $is_banned;
    public $is_deleted;
    public $must_change_pass;
    public $hear_us;
    public $last_login_by;
    public $created_at;
    public $updated_at;

    public $facebook_id;
    public $facebook_name;
    public $facebook_data;

    public $google_id;
    public $google_name;
    public $google_data;

    public $linkedin_id;
    public $linkedin_name;
    public $linkedin_data;

    public $microsoft_id;
    public $microsoft_name;
    public $microsoft_data;

    public $github_id;
    public $github_name;
    public $github_data;


    protected $providers = ["normal", "google", "facebook", "microsoft", "github", "linkedin"];

    public function initialize ()
    {
        parent::initialize();
        $this->setSource("members");

        $this->addBehavior(new Timestampable(
            [
                'beforeCreate' => [
                    'field'  => 'created_at',
                    'format' => 'Y-m-d H:i:s'
                ],
                'beforeUpdate' => [
                    'field'  => 'updated_at',
                    'format' => 'Y-m-d H:i:s'
                ],
            ]
        ));
    }

    public function validation ()
    {
        $this->validate(new Email([
            'field'   => 'identity',
            "message" => "Invalid email address"
        ]));

        $this->validate(new Uniqueness([
            "field"   => "email",
            "message" => "The email address is exist"
        ]));

        $this->validate(new InclusionIn([
            "field"  => "status",
            "domain" => ["normal", "resend"]
        ]));

        $this->validate(new InclusionIn([
            "field"  => "role",
            "domain" => ["guest", "member", "operator", "admin"]
        ]));

        $this->validate(new InclusionIn([
            "field"  => "last_login_by",
            "domain" => $this->providers
        ]));

        return $this->validationHasFailed() != true;
    }

    public function beforeValidationOnCreate ()
    {
        $this->salt = sha1(base64_encode(openssl_random_pseudo_bytes(80)));
        $hash       = sha1($this->password . $this->salt);

        //Use this password as default
        $this->password = $this->getDI()->getSecurity()->hash($hash);

        if (empty($this->status)) {
            $this->status = 'normal';
        }
        if (empty($this->role)) {
            $this->role = 'member';
        }
        if (empty($this->is_active)) {
            $this->is_active = 0;
        }
        if (empty($this->is_banned)) {
            $this->is_banned = 0;
        }
        if (empty($this->is_deleted)) {
            $this->is_deleted = 0;
        }
    }

    /**
     * Send a confirmation e-mail to the user after create the account
     */
    public function afterCreate ()
    {
        $this->sendConfirmationCode();
    }

    /**
     * Send confirmation email
     */
    public function sendConfirmationCode ()
    {
        $tempCode   = new TempCodes();
        $this->code = $tempCode->generateCode($this->id, TempCodes::CONFIRM);

        $m = new \Tartan\Mail\Message();

        $m->setSubject('Account Confirmation')
            ->setTemplate('confirmation')
            ->setTemplateParameters([
                'confirmUrl' => $this->getDI()->url->setBaseUri('site/user/confirm/?code=' . urlencode($this->code)),
                'code'       => $this->code
            ])->addTo($this->getEmail(), $this->getName());

        return $this->getDI()->mail->send($m);
    }

    /**
     * Change user password
     *
     * @param string $newPassword
     * @param string $oldPassword
     */
    public function changePassword ($newPassword, $oldPassword)
    {
        $checkOldPass = $this->auth->getIdentity()['role'] == 'admin' ? 'false' : 'true';

        if ($checkOldPass) {
            if ($this->auth->getIdentity()['id'] != $this->getId()) {
                throw new \Exception ("You don't have access to change this user's password");
            }

            $oldHash     = hash_hmac('sha1', $oldPassword, $this->salt);
            $oldPassword = $this->getDI()->getSecurity()->hash($oldHash);

            if ($oldPassword != $this->password) {
                throw new \Exception ("Old password is not valid!");
            }
        }

        $newHash        = hash_hmac('sha1', $newPassword, $this->salt);
        $this->password = $this->getDI()->getSecurity()->hash($newHash);

        return $this->save();
    }


    // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ //
    public function setId ($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getId ()
    {
        return $this->id;
    }

    public function setName ($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName ()
    {
        if ($this->name) {
            return $this->name;
        }

        $providers = $this->providers;
        unset ($providers['normal']);
        $name = '';
        foreach ($providers as $provider) {
            if ($this->getProviderData($providers) && is_object(unserialize($this->getProviderData($providers)))) {
                $data = unserialize($this->getProviderData($providers));
                if (isset($data->name) && trim($data->name) != '') {
                    $name = $data->name;
                    break;
                }
            }
        }
        return $name;
    }

    public function setIdentity ($identity)
    {
        $this->identity = $identity;

        return $this;
    }

    public function getIdentity ()
    {
        return $this->identity;
    }

    public function setEmail ($email)
    {
        $this->identity = $email;

        return $this;
    }

    public function getEmail ()
    {
        if ($this->identity) {
            return $this->identity;
        }

        $providers = $this->providers;
        unset ($providers['normal']);
        $email = '';
        foreach ($providers as $provider) {
            if ($this->getProviderData($providers) && is_object(unserialize($this->getProviderData($providers)))) {
                $data = unserialize($this->getProviderData($providers));
                if (isset($data->email) && trim($data->email) != '') {
                    $email = $data->email;
                    break;
                }
            }
        }
        return $email;
    }

    public function getPassword ()
    {
        return $this->password;
    }

    public function setPassword ($password)
    {
        $this->password = $password;

        return $this;
    }

    public function getSalt ()
    {
        return $this->salt;
    }

    public function setSalt ($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    public function getRole ()
    {
        return $this->role;
    }

    public function setRole ($role)
    {
        $this->role = $role;

        return $this;
    }

    public function getStatus ()
    {
        return $this->status;
    }

    public function setStatus ($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt ()
    {
        return $this->created_at;
    }

    public function setCreatedAt ($date)
    {
        $this->created_at = $date;

        return $this;
    }

    public function getUpdated ()
    {
        return $this->updated_at;
    }

    public function setUpdatedAt ($date)
    {
        $this->updated_at = $date;

        return $this;
    }


    public function setIsActive ($flag)
    {
        $this->is_active = (int) $flag;

        return $this;
    }

    public function getIsActive ()
    {
        return $this->is_active;
    }

    public function setIsBanned ($flag)
    {
        $this->is_banned = (int) $flag;

        return $this;
    }

    public function getIsBanned ()
    {
        return $this->is_banned;
    }

    public function setIsDeleted ($flag)
    {
        $this->is_deleted = (int) $flag;

        return $this;
    }

    public function getIsDeleted ()
    {
        return $this->is_deleted;
    }

    public function setMustChangePassword ($flag)
    {
        $this->must_chpass = $flag;

        return $this;
    }
    public function getMustChangePassword ()
    {
        return $this->must_change_pass;
    }

    public function setHearUs($how) {
        $this->hear_us = $how;
        return $this;
    }

    public function getHearUs() {
        return $this->hear_us;
    }

    public function setLastLoginBy($provider) {
        $this->last_login_by = strtolower($provider);
        return $this;
    }

    public function getLastLoginBy() {
        return $this->last_login_by;
    }

    public function isOauthUser ()
    {
        $providers = $this->providers;
        unset ($providers['normal']);
        $oauth = false;
        foreach ($providers as $provider) {
            if ($this->getProviderId($providers)) {
                $oauth = true;
                break;
            }
        }
        return $oauth;
    }

    public function getImageUrl ()
    {
        $providers = $this->providers;
        unset ($providers['normal']);
        $url = '';
        foreach ($providers as $provider) {
            if ($this->getProviderData($providers) && is_object(unserialize($this->getProviderData($providers)))) {
                $data = unserialize($this->getProviderData($providers));
                if (isset($data->imageUrl) && trim($data->imageUrl) != '') {
                    $url = $data->imageUrl;
                    break;
                }
            }
        }
        return $url;
    }

    # Providers --------------------------------------------------------------------------------------------------------
    public function setProviderId ($provider, $id)
    {
        $provider == 'normal' ?
            $property = 'id' :
            $property = strtolower($provider)."_id";
        $this->$property = $id;

        return $this;
    }

    public function getProviderId ($provider)
    {
        $provider == 'normal' ?
            $property = 'id' :
            $property = strtolower($provider)."_id";
        return $this->$property;
    }

    public function setProviderName ($provider, $name)
    {
        if (trim($name) != '') {
            $provider == 'normal' ?
                $property = 'name' :
                $property = strtolower($provider)."_name";
            $this->$property = $name;
        }
        return $this;
    }

    public function getProviderName ($provider)
    {
        $provider == 'normal' ?
            $property = 'name' :
            $property = strtolower($provider)."_name";
        return $this->$provider;
    }

    public function setProviderData ($provider, $data)
    {
        if ($provider == 'normal') {
            return $this;
        }

        $property = strtolower($provider)."_data";
        $this->$property = $data;

        return $this;
    }

    public function getProviderData ($provider)
    {
        if ($provider == 'normal') {
            return $this;
        }
        $property = strtolower($provider)."_data";
        return $this->$property;
    }
}