<?php

namespace Tartan\Auth\Model;

use Tartan\Model;

/**
 * RememberTokens
 *
 * Stores the remember me tokens
 */
class RememberTokens extends Model
{
    public $id;
    public $fk_member_id;
    public $token;
    public $user_agent;
    public $created_at;

    /**
     * Before create the user assign a password
     */
    public function beforeValidationOnCreate ()
    {
        //Timestamp the confirmation
        $this->created_at = time();
    }

    public function initialize ()
    {
        parent::initialize();
        $this->setSource("remember_tokens");

        $this->belongsTo('fk_member_id', 'Tartan\Auth\Model\Members', 'id', array(
            'alias' => 'member'
        ));
    }

}