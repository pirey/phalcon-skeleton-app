<?php

/*
 * Copyright 2014 Aboozar Ghaffari
 * mail: me@tartan.pro
 *
 */
namespace Tartan\Navigation;

class Navigation
{
    /**
     * Collection of navigations
     *
     * @var array
     */
    private $_navigations = array();

    /**
     * Default Renderer class in \Navigation\Renderer path
     * @var string
     */
    private $renderer = 'DefaultRenderer';

    /**
     * Build all navigation with configuration
     *
     * @param array $navigation
     */
    public function __construct($navigation = [])
    {
        if (!empty($navigation)) {
            foreach ($navigation as $navName => $navData) {
                $this->_navigations[$navName] = $this->mapNode($navData);
            }
        }
    }

    /**
     * set Navigation params
     *
     * @param array $navigation
     */
    public function setNavigation($navigation = [])
    {
        if (!empty($navigation)) {
            foreach ($navigation as $navName => $navData) {
                $this->_navigations[$navName] = $this->mapNode($navData);
            }
        }
    }

    /**
     * Return all navigation collection
     *
     * @param type $name
     * @return array
     */
    public function getNavigation($name) {
        if (!array_key_exists($name, $this->_navigations))
            return null;

        return $this->_navigations[$name];
    }

    public function setRenderer($renderer) {
        $this->renderer = $renderer;
        return $this;
    }

    /**
     * Generate HTML code
     *
     * @param string $name
     * @param string|type $renderer
     * @throws \Exception
     * @return type
     */
    public function toHtml($name, $renderer = null) {

        if (!($renderer)) {
            $renderer = __NAMESPACE__ .'\\Renderer\\'.ucfirst($name);
        }

        if (is_string($this->renderer)) {
            $renderer = new $renderer();
        }

        if (!($renderer instanceof \Tartan\Navigation\Renderer\RendererAbstract)) {
            throw new \Exception ('Navigation renderer class should be instance of
                \Tartan\Navigation\Renderer\RendererAbstract');
        }
        if (!$this->getNavigation($name)) {
            throw new \Exception ("'{$name}' navigation data not found!");
        }
        return $renderer->toHtml($this->getNavigation($name));
    }

    /**
     * Collection mapper
     *
     * @param type $childrenData
     * @param type $parent
     * @return array
     */
    public function mapCollection($childrenData, $parent = null) {
        $collection = array();
        foreach ($childrenData as $nodeConfig) {
            $node = $this->mapNode($nodeConfig);
            $node->setParent($parent);
            $collection[] = $node;
        }
        return $collection;
    }

    /**
     * Node mapper
     *
     * @param type $navData
     * @return \Tartan\Navigation\Node
     */
    public function mapNode(array $navData) {

        $node = new Node();
        foreach($navData as $key => $value){
            if($key == 'children')
                continue;

            $node->{'set'.ucfirst($key)}($value);
        }

        if (isset($navData['children'])) {
            $collection = $this->mapCollection($navData['children'], $node);
            $node->setChildren($collection);
        }

        return $node;
    }

    /**
     * Set active node in all navigation collections.
     *
     * @param type $action
     * @param type $controller
     * @param type $module
     * @return void
     */
    public function setActiveNode($module, $controller, $action, $routeName = null) {
        $this->dissactiveNodes();

        foreach ($this->_navigations as $navigation) {
            $this->_activeCollection($navigation->getChildren(), $module, $controller, $action, $routeName);
        }
    }

    /**
     * Activate all active nodes in collection.
     *
     * @param type $collection
     * @param type $action
     * @param type $controller
     * @param type $module
     * @return void
     */
    public function _activeCollection($collection, $module, $controller, $action, $routeName = null) {

        $uri = $_SERVER['REQUEST_URI'];
        foreach ($collection as $node) {
            if (
                ($routeName && $routeName == $node->getRoute()) ||
                ($uri && $uri == $node->getUrl()) ||
                ($node->getAction() == $action &&
                    $node->getController() == $controller &&
                        $node->getModule() == $module)
            ){

                $this->_activateNode($node);
            }

            if ($node->hasChildren())
                $this->_activeCollection($node->getChildren(), $action, $controller, $module);
        }
    }

    /**
     * Activate node
     *
     * @param type $node
     * @return void
     */
    private function _activateNode($node)
    {
        $node->setActive(true);
        if (!is_null($node->getParent()))
                $this->_activateNode ($node->getParent());
    }


    /**
     * Deactivate all nodes in collection
     *
     * @param type $collection
     */
    private function _dissactiveCollection($collection) {
        foreach ($collection as $node) {
            $node->setActive(false);
            if ($node->hasChildren())
                $this->dissactiveCollection($node->getChildren());
        }
    }

    /**
     * Deactivate all nodes in all collections
     */
    public function dissactiveNodes() {
        foreach ($this->_navigations as $navigation) {
            $this->_dissactiveCollection($navigation);
        }
    }

}
