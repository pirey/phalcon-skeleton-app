<?php
/*
 * Copyright 2014 Aboozar Ghaffari
 * mail: me@tartan.pro
 *
 */
namespace Tartan\Navigation;

class Node
{
    protected
        $name,
        $id,
        $class,
        $module,
        $controller,
        $action,
        $route,
        $routeParams,
        $url,
        $children,
        $target,
        $parent,
        $active;


    public function __call($method, $args)
    {
        if ( ! preg_match('/(?P<accessor>set|get)(?P<property>[A-Z][a-zA-Z0-9]*)/', $method, $match) ||
            ! property_exists(__CLASS__, $match['property'] = lcfirst($match['property']))
        ) {
            throw new \BadMethodCallException(sprintf(
                "'%s' does not exist in '%s'.", $method, get_class(__CLASS__)
            ));
        }


        switch ($match['accessor']) {
            case 'get':
                return $this->{$match['property']};
            case 'set':
                if ( ! $args) {
                    throw new \InvalidArgumentException(sprintf("'%s' requires an argument value.", $method));
                }
                $this->{$match['property']} = $args[0];
                return $this;
        }
    }

    /**
     * @return array
     */
    public function getRouteParams()
    {
        if (!empty($this->routeParams)) {
            return $this->routeParams;
        } else {
            return [];
        }
    }

    /**
     * Is node active?
     *
     * @return bool
     */
    public function isActive ()
    {
        return $this->active;
    }

    /**
     * Has node any children
     *
     * @return bool
     */
    public function hasChildren ()
    {
        return 0 < count($this->children);
    }

}
