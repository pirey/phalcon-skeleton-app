<?php

/*
 * Copyright 2014 Aboozar Ghaffari
 * mail: me@tartan.pro
 *
 */
namespace Tartan\Navigation\Renderer;

abstract class RendererAbstract
{

    /**
     * Html
     *
     * @var string
     */
    protected $html = '';

    /**
     * Translate
     *
     * @var object
     */
    protected $t;
    protected $url;

    /**
     * @todo add acl verification
     * @todo add schedule verification
     *
     */
    public  function __construct ()
    {
        if (is_null($this->t) && \Phalcon\DI::getDefault()->has('translate'))
            $this->t = \Phalcon\DI::getDefault()->get('translate');
        $this->url = \Phalcon\DI::getDefault()->get('url');
    }

    /**
     * Translate proxy
     *
     * @param type $word
     *
     * @return string
     */
    protected function _translate ($word)
    {
        if (is_null($this->t))
            return $word;

        return $this->t->_($word);
    }

    /**
     * Create ul elements
     *
     * @param type $node
     */
    protected function _generate ($node)
    {
        $class = !is_null($node->getClass()) ? " class='" . $node->getClass() . "' " : '';
        $id    = !is_null($node->getId()) ? " id='" . $node->getId() . "'" : '';

        $this->html .= "\t<ul$class$id>" . PHP_EOL;
        if ($node->hasChildren())
            $this->_generateChildren($node->getChildren());
        $this->html .= "\t</ul>" . PHP_EOL;
    }

    /**
     * Create children element
     *
     * @param type $children
     */
    protected function _generateChildren ($children)
    {
        foreach ($children as $node) {
            $this->_generateElement($node);
        }
    }

    /**
     * Create one element
     *
     * @param \Tartan\Navigation\Node $node
     */
    protected function _generateElement ($node)
    {
        $cssClasses = array();
        if ($node->isActive())
            $cssClasses[] = 'active';

        if (!is_null($node->getClass()))
            $cssClasses[] = $node->getClass();

        $class  = count($cssClasses) > 0 ? " class='" . implode(' ', $cssClasses) . "'" : '';
        $id     = !is_null($node->getId()) ? " id='" . $node->getId() . "'" : '';
        $target = !is_null($node->getTarget()) ? " target='" . $node->getTarget() . "'" : '';

        $this->html .= "\t\t<li$class $id>" . PHP_EOL;
        $this->html .= "\t\t\t<a title='" . $this->_translate($node->getName()) . "' href='" . $node->getUrl() . "' $target>" . $this->_translate($node->getName()) . "</a>" . PHP_EOL;

        //generate children
        if ($node->hasChildren()) {
            $display = $node->isActive() ? 'block' : 'none';
            $this->html .= "\t\t<ul class='children' style='display:$display'>" . PHP_EOL;
            $this->_generateChildren($node->getChildren());
            $this->html .= "\t\t</ul>" . PHP_EOL;
        }

        $this->html .= "\t\t</li>" . PHP_EOL;
    }

    /**
     * Generate all HTML
     *
     * @param type $node
     *
     * @return string
     */
    public function toHtml ($node)
    {
        $this->_generate($node);

        return $this->html;
    }

}
