<?php
namespace Tartan\Helper;

class Persian
{
    protected static $farsiArray   = array("۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹");
    protected static $arabicArray  = array("٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩");
    protected static $englishArray = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");

    protected static $nonPersianArray  = array ("ى", "ي", "ك", "ئ", "إ", "أ", "ٱ", "ة", "ؤ", "ء");
    protected static $persianArray     = array ("ی", "ی", "ک", "ی", "ا", "ا", "ﺍ", "ه", "و", "");

    public static function toFa($numberString)
    {
        $tmp = str_replace(self::$englishArray, self::$farsiArray, $numberString);
        return str_replace(self::$arabicArray, self::$farsiArray, $tmp);
    }

    public static function toEn($numberString)
    {
        $tmp = str_replace(self::$farsiArray, self::$englishArray, $numberString);
        return str_replace(self::$arabicArray, self::$englishArray, $tmp);
    }

    public static function normalizeFa($string)
    {
        return str_replace(self::$nonPersianArray, self::$persianArray, $string);
    }

    public static function aggressiveFa($string)
    {
        self::$nonPersianArray [] = 'آ';
        self::$persianArray []    = 'ا';

        return str_replace(self::$nonPersianArray, self::$persianArray, $string);
    }

    // Only Persian alphabet
    public static function forceFa($string) {
        return preg_replace("/[^\x{0627}-\x{0628}\x{062A}-\x{0648}\x{06F0}-\x{06F9} \x{0622}\x{06A9}\x{06AF}\x{067E}\x{0698}\x{06CC}\x{0686}]/u", '', $string);
    }

    public static function normalizeSpace ($string, $replace = ' ') {
        return  preg_replace('!\s+!', $string, trim($string));
    }
}