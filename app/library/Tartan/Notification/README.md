Tartan\Notification - Notifications for PhalconPHP applications
===

This is a simple little add on that provides some simple functions to implement a notification system in web applications.

Using this is really simple. I recommend registering it with the services for easier access

```php
$di->set('notify', function() {
    $notify = new \Tartan\Notification\Notifications();
    return $notify;
});
```

From this point on you can access it through ```$this->notify``` in all controllers, views, etc.

## Set up the database table

  
```sql
CREATE TABLE `notifications` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `section` VARCHAR(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subsection` VARCHAR(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recipient` INT(11) UNSIGNED DEFAULT NULL,
  `message` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `redirect` VARCHAR(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seen` TINYINT(1) DEFAULT NULL,
  `created_at` DATETIME DEFAULT NULL,
  `updated_at` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
```

## Usage

Create a new notification and store it in the database

```php
$message = 'John Smith just left a comment on your gallery';
$this->notify->add(1, $message, 'gallery', '56823', '/gallery/56823');
```

Get all unseen notifications
```php
$notifications = $this->notify->findByRecipient(1);
foreach ($notifications as $notification) {
    // ...
}
```

Get all notifications including seen
```php
$notifications = $this->notify->findByRecipient(1, true);
foreach ($notifications as $notification) {
    // ...
}
```

Mark notification as seen
```php
$this->notify->mark(8);
```
This will mark notification with ID 8 as seen