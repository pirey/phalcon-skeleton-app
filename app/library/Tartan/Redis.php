<?php
namespace Tartan;

class Redis
{
    /**
     * @var Predis\Client
     */
    public $client;

    public function set($key, $value)
    {
        return $this->client->set($key, serialize($value));
    }

    public function get($key)
    {
        $val = $this->client->get($key);
        return $val ? unserialize($val) : null;
    }
}