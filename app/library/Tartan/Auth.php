<?php
namespace Tartan;

use Phalcon\Mvc\User\Component;
use Tartan\Auth\Model\Members;
use Tartan\Auth\Model\FailedLogins;
use Tartan\Auth\Model\RememberTokens;
use Tartan\Auth\Model\SuccessLogins;
use Tartan\Auth\Exception;
use League\OAuth2\Client\Entity\User;

/**
 * Tartan\Auth
 *
 * Manages Authentication/Identity Management in Eagle
 */
class Auth extends Component
{
    /**
     * Checks the user credentials
     *
     * @param $credentials
     *
     * @throws \Tartan\Auth\Exception
     */
    public function loginNormal ($credentials)
    {
        // Check if the user exist
        $member = Members::findFirstByIdentity($credentials['email']);

        // member not found
        if ($member == false) {
            $this->registerUserThrottling(0);
            throw new Exception('Wrong email/password combination');
        }

        // Check the password
        $hash = sha1($credentials['password'] . $member->getSalt());

        // Authentication
        if (!$this->security->checkHash($hash, $member->getPassword())) {
            $this->registerUserThrottling($member->getId());
            throw new Exception('Wrong email/password combination');
        }

        //Check if the user was flagged
        $this->checkUserFlags($member);

        //Register the successful login
        $this->saveSuccessLogin($member);

        //Check if the remember me was selected
        if (isset($credentials['remember'])) {
            $this->createRememberEnvironment($member);
        }

        //Register identity in session
        $this->setIdentity($member);
        if ($this->session->has('_redirectUrl'))
        {
            $_redirectUrl = $this->session->get('_redirectUrl');
            $this->session->remove('_redirectUrl');
            return $this->response->redirect($_redirectUrl, true);
        }

        return $this->response->redirect(['for' => 'dashboard'], true);
    }

    /**
     * Logs on using the information in the coookies
     *
     * @return \Phalcon\Http\Response
     */
    public function loginWithRememberMe ()
    {
        $memberId    = $this->cookies->get('RMU')->getValue();
        $cookieToken = $this->cookies->get('RMT')->getValue();

        $member = Members::findFirstById($memberId);

        if ($member)
        {
            $userAgent = $this->request->getUserAgent();

            // generate unique token
            $token = generateRememberToken($member);

            if ($cookieToken == $token)
            {
                $remember = RememberTokens::findFirst([
                    'fk_member_id = ?0 AND token = ?1',
                    'bind' => [$member->getId(), $token]
                ]);

                if ($remember)
                {
                    //Check if the cookie has not expired
                    if ((time() - (86400 * 8)) < $remember->getCreatedAt()) {

                        //Check if the member was flagged
                        $this->checkUserFlags($member);

                        //Register identity in session
                        $this->setIdentity($member);

                        //Register the successful login
                        $this->saveSuccessLogin($member);

                        if ($this->session->has('_redirectUrl'))
                        {
                            $_redirectUrl = $this->session->get('_redirectUrl');
                            $this->session->remove('_redirectUrl');
                            return $this->response->redirect($_redirectUrl, true);
                        }
                        return $this->response->redirect(['for' => 'dashboard'], true);
                    }
                }
            }
        }

        $this->cookies->get('RMU')->delete();
        $this->cookies->get('RMT')->delete();

        return $this->response->redirect(['for' => 'signin'], true);
    }

     public function loginWithOAuth2 (User $userEntity, $provider)
    {
        $member = Members::findFirst(array(
            sprintf("%s_id = :%sID: OR identity = :Identity:", strtolower($provider), $provider),
            "bind" => [
                sprintf('%sID', $provider) => $userEntity->uid,
                'Identity' => $userEntity->email
            ]
        ));

        if ($member) {
            $this->checkUserFlags($member);
            $this->setIdentity($member);

            if (!$member->{"get".$provider."Id"}())
            {
                $member->{"set".$provider."Id"}($userEntity->uid);
                $member->{"set".$provider."Name"}($userEntity->name);
                $member->{"set".$provider."Data"}(serialize($userEntity));
                $member->{"set".$provider."Data"}(serialize($userEntity));
                $member->setLastLoginBy($provider);
                $member->update();
            }

            $this->saveSuccessLogin($member);

            if ($this->session->has('_redirectUrl'))
            {
                $_redirectUrl = $this->session->get('_redirectUrl');
                $this->session->remove('_redirectUrl');
                return $this->response->redirect($_redirectUrl, true);
            }
            return $this->response->redirect(['for' => 'dashboard'], true);
        }
        else {
            $password = $this->generatePassword();

            $member = new Members();
            $member->setIdentity($userEntity->email);
            $member->setPassword($password);
            $member->{"set".$provider."Id"}($userEntity->uid);
            $member->{"set".$provider."Name"}($userEntity->name);
            $member->{"set".$provider."Data"}(serialize($userEntity));
            $member->setMustChangePassword(0);
            $member->setLastLoginBy($provider);
            $member->setIsActive(true); // Email activation not required

            if (true == $member->create())
            {
                $this->setIdentity($member);
                $this->saveSuccessLogin($member);

                if ($this->session->has('_redirectUrl'))
                {
                    $_redirectUrl = $this->session->get('_redirectUrl');
                    $this->session->remove('_redirectUrl');
                    return $this->response->redirect($_redirectUrl, true);
                }
                return $this->response->redirect(['for' => 'dashboard'], true);
            }
            else {
                foreach ($member->getMessages() as $message) {
                    $this->flashSession->error($message->getMessage());
                }

                return $this->response->redirect(['for' => 'signin'], true);
            }
        }
    }

    /**
     * Creates the remember me environment settings the related cookies and generating tokens
     *
     * @param Members $member
     *
     * @throws \Tartan\Auth\Exception
     */

    public function saveSuccessLogin (Members $member)
    {
        $successLogin               = new SuccessLogins();
        $successLogin->fk_member_id = $member->getId();
        $successLogin->ip_address   = $this->request->getClientAddress();
        $successLogin->user_agent   = $this->request->getUserAgent();
        if (!$successLogin->save()) {
            $messages = $successLogin->getMessages();
            throw new Exception($messages[0]);
        }
    }

    /**
     * Implements login throttling
     * Reduces the effectiveness of brute force attacks
     *
     * @param int $memberId
     */
    public function registerUserThrottling ($memberId)
    {
        $failedLogin               = new FailedLogins();
        $failedLogin->fk_member_id = $memberId;
        $failedLogin->ip_address   = $this->request->getClientAddress();
        $failedLogin->attempted    = time();
        $failedLogin->save();

        $attempts = FailedLogins::count(array(
            'ip_address = ?0 AND attempted >= ?1',
            'bind' => array(
                $this->request->getClientAddress(),
                time() - 3600 * 6 // 6 hours
            )
        ));

        switch ($attempts) {
            case 1:
            case 2:
                // no delay
                break;
            case 3:
            case 4:
                sleep(2);
                break;
            default:
                sleep(4);
                break;
        }

    }

    /**
     * Creates the remember me environment settings the related cookies and generating tokens
     *
     * @param Members $member
     */

    public function createRememberEnvironment (Members $member)
    {
        $token = $this->generateRememberToken ($member);

        $remember               = new RememberTokens();
        $remember->fk_member_id = $member->getId();
        $remember->token        = $token;
        $remember->user_agent   = $this->request->getUserAgent();

        if ($remember->save() != false) {
            $expire = time() + 86400 * 7; // 1 week
            $this->cookies->set('RMU', $member->getId(), $expire);
            $this->cookies->set('RMT', $token, $expire);
        }
    }

    /**
     * Check if the session has a remember me cookie
     *
     * @return boolean
     */
    public function hasRememberMe ()
    {
        return $this->cookies->has('RMU');
    }

    /**
     * Checks if the user is banned/inactive/deleted
     *
     * @param Members $member
     *
     * @throws \Tartan\Auth\Exception
     */
    public function checkUserFlags (Members $member)
    {
        if (!$member->getIsActive()) {
            throw new Exception('The user is inactive');
        }

        if ($member->getIsBanned()) {
            throw new Exception('The user is banned');
        }

        if ($member->getIsDeleted()) {
            throw new Exception('The user is deleted');
        }
    }

    /**
     * Set identity in session
     *
     * @param object $member
     */
    private function setIdentity ($member)
    {
        $st_identity = array(
            'id'       => $member->getId(),
            'identity' => $member->getIdentity(),
            'email'    => $member->getEmail(),
            'name'     => $member->getForcedName(),
            'role'     => $member->getRole()
        );

        if ($member->getProfile()) {
            $st_identity['profile_picture'] = $member->getProfile()->getImageUrl();
        }

        $this->session->set('auth-identity', $st_identity);
    }

    /**
     * Returns the current identity
     *
     * @return array
     */
    public function getIdentity ()
    {
        return $this->session->get('auth-identity');
    }

    /**
     * Returns if user has session or not
     *
     * @return array
     */
    public function hasIdentity ()
    {
        return $this->session->has('auth-identity');
    }

    /**
     * Removes the user identity information from session
     */
    public function remove ()
    {
        if ($this->cookies->has('RMU')) {
            $this->cookies->get('RMU')->delete();
        }
        if ($this->cookies->has('RMT')) {
            $this->cookies->get('RMT')->delete();
        }

        $this->session->remove('auth-identity');
    }

    /**
     * Get the entity related to user in the active identity
     *
     * @return bool|Members
     *
     * @throws \Tartan\Auth\Exception
     */
    public function getUser ()
    {
        $identity = $this->session->get('auth-identity');
        if (isset($identity['id'])) {
            $user = Members::findFirstById($identity['id']);
            if ($user == false) {
                throw new Exception('The user does not exist');
            }

            return $user;
        }

        return false;
    }

    /**
     * Returns a key value from user session
     *
     * @param string $key
     *
     * @return string|null
     */
    protected function getAuthSessionKey ($key = null)
    {
        if ($this->hasIdentity()) {
            $auth = $this->getIdentity();
            if ($key) {
                if (isset($auth[$key])) {
                    return $auth[$key];
                }
                else {
                    return null;
                }
            }
        }
        else {
            return null;
        }
    }

    /**
     * Generate a random password
     *
     * @param integer $length
     *
     * @return string
     */
    public function generatePassword ($length = 8)
    {
        $chars = "abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ123456789#@%_.";

        return substr(str_shuffle($chars), 0, $length);
    }

    public function generateRememberToken(Members $member)
    {
        return sha1($member->getIdentity() . $member->getSalt().  $member->password . $this->request->getUserAgent());
    }
}