<?php
namespace Tartan;
use Phalcon\DI;

class Model extends \Phalcon\Mvc\Model
{
    public function initialize ()
    {
        $this->setConnectionService(DI::getDefault()->get('config')->tartan->db);
    }
}