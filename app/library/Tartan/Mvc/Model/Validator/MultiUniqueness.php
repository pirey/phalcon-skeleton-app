<?php
/**
 * Created by PhpStorm.
 * Package: Tartan
 * User: aboozar <me@tartan.pro>
 * Date: 1/31/14
 * Time: 10:28 PM
 */
namespace Tartan\Mvc\Model\Validator;

use Phalcon\Mvc\Model\Validator,
    Phalcon\Mvc\Model\ValidatorInterface,
    Phalcon\Validation\Message;

class MultiUniqueness extends Validator implements ValidatorInterface
{
    public function validate($record)
    {
        $fields = $this->getOption('fields');
        if (!is_array($fields)) {
            $fields = [$fields];
        }

        var_dump($fields, $record);exit;

        $value = $record->readAttribute($fields);
        $users = Users::find(array(
            "conditions" => array("name" => $value)
        ));

        if(count($users) == 1)
        {
            $this->appendMessage("The Name is already in use", $field, "Unique");
            return false;
        }
        return true;
    }
}