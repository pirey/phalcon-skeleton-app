<?php
namespace Tartan\Mvc;

use Phalcon\Mvc\Dispatcher;

class AuthController extends Controller
{
    public function beforeExecuteRoute (Dispatcher $dispatcher)
    {
        if (isset($this->config->tartanAuth)) {
            $acl            = $this->acl->getAcl();
            $moduleName     = $dispatcher->getModuleName();
            $controllerName = $dispatcher->getControllerName();
            $actionName     = $dispatcher->getActionName();
            if (empty($moduleName)) {
                $moduleName = 'default';
            }

            //Get the current identity
            $identity = $this->auth->getIdentity();

            //If there is no identity available the user is redirected to index/index
            if (!is_array($identity)) {
                $role = $this->config->tartanAuth->defaultRole;
            }
            else {
                $role = $this->auth->getIdentity()['role'];
            }

            $resources = array(
                array('*/*', '*'),
                array('*/' . $controllerName, '*'),
                array('*/*', $actionName),
                array($moduleName . '/*', '*'),
                array($moduleName . '/' . $controllerName, '*'),
                array($moduleName . '/' . $controllerName, $actionName)
            );

            $hasAccess = false;
            foreach ($resources as $res) {
                if ($acl->isResource($res[0])) {
                    $result = $acl->isAllowed($role, $res[0], $res[1]);

                    if ($result == true) {
                        $hasAccess = true;
                        break 1;
                    }
                }
            }

            if (!$hasAccess) {
                $this->flashSession->notice('You (role: ' . $role . ') don\'t have access to this resource: ' . $moduleName . '/' . $controllerName . ':' . $actionName);

                if ($acl->isAllowed($role, $moduleName . '/' . $controllerName, 'index')) {
//                $dispatcher->forward(array(
//                    'module'     => 'site',
//                    'controller' => $controllerName,
//                    'action'     => 'index'
//                ));
                    $dispatcher->redirect("site/{$controllerName}/signin");
                }
                else {
                    $this->response->redirect("site/user/signin");
                }

                return false;
            }
        }
    }
}