<?php
namespace Tartan\Mvc;

use Phalcon\Mvc\Dispatcher;
use Phalcon\DI;

class Controller extends \Phalcon\Mvc\Controller
{
    public function initialize()
    {
        if ($this->request->isAjax()) {
            // disable layout
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        }
    }

    public static function _($message)
    {
        return DI::getDefault()->get('t9n')->_($message);
    }
}