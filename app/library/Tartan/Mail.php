<?php

namespace Tartan;

use Phalcon\Mvc\User\Component,
    Tartan\Mail\Exception as Exception,
    Tartan\Mandrill,
    Swift_Message as SwiftMessage,
    Swift_SmtpTransport as SmtpTransport,
    Swift_Mailer;

/**
 * Eagle\Mail\Mail
 *
 * Sends e-mails based on pre-defined templates
 */
class Mail extends Component
{

    const MANDRILL = 'mandrill';
    const SMTP     = 'smtp';

    protected $_transport;
    protected $settings;

    /**
     * @param mixed $setting
     */
    public function __construct($settings = array())
    {
        if ($settings instanceof \Phalcon\Config) {
            $settings = $settings->toArray();
        }

        if (!is_array($settings)) {
            throw new Exception(
                'Tartan Mail parameters must be in an array or \Phalcon\Config object'
            );
        }

        $this->settings  = $settings;
    }

    public function send(\Tartan\Mail\Message $message, $adapter = 'mandrill')
    {
        if ($message->ready()) {
            return $this->$adapter($message);
        }
        return false;
    }


    private function smtp(\Tartan\Mail\Message $message)
    {
        require_once APPLICATION_PATH . '/vendor/Swift/swift_required.php';

        $to  = array();
        $cc  = array();
        $bcc = array();

        foreach ($message->getTo() as $recipient)
        {
            $type = $recipient ['type'];
            array_push($$type, array($recipient['email'] => $recipient->name));
        }

        // Create the message
        $swiftMessage = SwiftMessage::newInstance()
            ->setSubject($message->getSubject())
            ->setTo($to)
            ->setFrom(array(
                $this->settings['mail']['fromEmail'] => $this->settings['mail']['fromName']
            ))
            ->setBody($message->getBody(), 'text/html');

        if ($cc) {
            $swiftMessage->setCc($cc);
        }

        if ($bcc) {
            $swiftMessage->setBcc($bcc);
        }

        if (!$this->_transport)
        {
            $this->_transport = SmtpTransport::newInstance(
                $this->settings['mail']['smtp']['server'],
                $this->settings['mail']['smtp']['port'],
                $this->settings['mail']['smtp']['security']
            )
                ->setUsername($this->settings['mail']['smtp']['username'])
                ->setPassword($this->settings['mail']['smtp']['password']);
        }

        // Create the Mailer using your created Transport
        $mailer = Swift_Mailer::newInstance($this->_transport);

        $result = $mailer->send($swiftMessage);

        if (!$result) {
            throw new Exception('Error sending email from Mandrill');
        }

        return true;
    }

    private function mandrill(\Tartan\Mail\Message $message)
    {
        $mandrill = new Mandrill($this->settings['mail']['mandrill_api_key']);

        //Prepare email!
        $email = array(
            'html'       => $message->getBody(), //Consider using a view file
            'text'       => strip_tags($message->getBody()),
            'subject'    => $message->getSubject(),
            'from_email' => $this->settings['mail']['fromEmail'],
            'from_name'  => $this->settings['mail']['fromName'],
            'to'         => $message->getTo() //Check documentation for more details on this one
            //'to'       => array(array('email' => 'joe@example.com' ),array('email' => 'joe2@example.com' )) //for multiple emails
        );

        //Send email!
        $result = current($mandrill->messages_send($email));

        if ($result['status'] == 'error') {
            // sending failed
            throw new Exception('Error sending email from Mandrill: '. $result['message']);
        } else if (isset($result['reject_reason']) && !empty($result['reject_reason'])) {
            // email rejected
            throw new Exception('Error sending email from Mandrill: '. $result['reject_reason']);
        }

        return true;
    }

    /**
     * Send a raw e-mail via AmazonSES
     *
     * @param string $raw
     */
    private function amazon($message)
    {
        require_once APPLICATION_PATH . '/vendor/AWSSDKforPHP/sdk.class.php';

        if ($this->_amazonSes == null) {
            $this->_amazonSes = new \AmazonSES(
                $this->settings['mail']['amazon']['AWSAccessKeyId'],
                $this->settings['mail']['amazon']['AWSSecretKey']
            );
            $this->_amazonSes->disable_ssl_verification();
        }

        $response = $this->_amazonSes->send_raw_email(array(
            'Data' => base64_encode($message)
        ), array(
            'curlopts' => array(
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0
            )
        ));

        if (!$response->isOK()) {
            throw new Exception('Error sending email from AWS SES: ' . $response->body->asXML());
        }

        return true;
    }
}
