<?php
namespace Tartan\Mail;

use Phalcon\Mvc\View,
    Phalcon\Mvc\User\Component;

class Message extends Component
{
    protected $subject;
    protected $text;
    protected $html;
    protected $fromName;
    protected $fromEmail;
    protected $templateName;
    protected $templateParams;
    protected $to = array();


    public $settings;

    public function __construct($settings = null)
    {
        if ($settings) {
            $this->settings = $settings;
        }
    }

    /**
     * Applies a template to be used in the e-mail
     *
     * @param string $name
     * @param array $params
     */
    public function renderTemplate($name, $params)
    {
        $parameters = array_merge(array(
            'publicUrl' => $this->settings['application']['publicUrl'],
        ), $params);

        return $this->view->getRender(
            'emailTemplates',
            $name,
            $parameters,
            function($view)
            {
                $view->setViewsDir(APPLICATION_PATH . '/app/views/');
                $view->setLayout('emailTemplates');
                $view->setRenderLevel(View::LEVEL_MAIN_LAYOUT);
                return $view;
            }
        );
    }

    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Sample usage
     * string: john@domain.com
     * array : ['John Smith', 'john@domain.com']
     * @param array|string $from
     * @param string $fromName
     */
    public function setFrom($from, $fromName = null)
    {
        if (is_array($from)) {
            list($this->fromName, $this->fromEmail) = $from;
        } else {
            $this->fromName  = $fromName;
            $this->fromEmail = $from;
        }
        return $this;
    }

    public function setTemplate($template)
    {
        $this->templateName = $template;
        return $this;
    }

    public function getTemplate()
    {
        return $this->templateName;
    }

    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setHtml($html)
    {
        $this->html = $html;
        return $this;
    }

    public function getHtml()
    {
        return $this->html;
    }

    public function setTemplateParameters(array $params)
    {
        $this->templateParams = $params;
        return $this;
    }

    public function getTemplateParameters()
    {
        return $this->templateParams;
    }

    public function addTo($toEmail, $toName = '')
    {
        $this->to [] = array (
            'email' => $toEmail,
            'name'  => $toName,
            'type'  => 'to'
        );

        return $this;
    }

    public function addCc($toEmail, $toName = '')
    {
        $this->to [] = array (
            'email' => $toEmail,
            'name'  => $toName,
            'type'  => 'cc'
        );

        return $this;
    }

    public function addBcc($toEmail, $toName = '')
    {
        $this->to [] = array (
            'email' => $toEmail,
            'name'  => $toName,
            'type'  => 'bcc'
        );

        return $this;
    }


    public function getBody()
    {
        if ($this->html) {
            return $this->html;
        } elseif ($this->templateName) {
            return $this->renderTemplate($this->templateName, $this->templateParams);
        } else if ($this->text) {
            return $this->text;
        } else {
            throw new Exception ('Tartan message body not found!');
        }
    }

    public function setTo(array $to)
    {
        $this->to = $to;
        return $this;
    }

    public function getTo()
    {
        return $this->to;
    }

    public function ready()
    {
        $subject   = $this->getSubject();
        $body      = $this->getBody();
        $recipient = $this->getTo();

        if (empty($subject)) {
            throw new Exception ('Tartan message should have a `subject`');
        } elseif (empty($body)) {
            throw new Exception ('Tartan message should have a `body` (html/template/text)');
        } elseif (empty($recipient)) {
            throw new Exception ('Tartan message should have at least one `recipient`');
        }

        return true;
    }
}