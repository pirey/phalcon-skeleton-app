<?php
/**
 * @package Tartan Framework (extended PhalconPhp Framework)
 * @namespace Tartan\Validation\Validator
 * @class Tartan\Validation\Validator\Captcha
 * @author  Tartan <me[@]tartan[.]pro>
 * @link http://git.tartan.pro
 * @license No license. Completely free, enjoy ;)
 */

namespace Tartan\Validation\Validator;

use Phalcon\Validation\Validator,
    Phalcon\Validation\ValidatorInterface,
    Phalcon\Validation\Message,
    Phalcon\DI;

class Captcha extends Validator implements ValidatorInterface
{
    /**
     * Executes the validation
     *
     * @param Phalcon\Validation $validator
     * @param string $attribute
     * @return boolean
     */
    public function validate($validator, $attribute)
    {
        $value = $validator->getValue($attribute);

        $di = DI::getDefault();
        $session = $di->getShared('session');
        $session_var = (new \Tartan\Captcha\Captcha())->session_var;

        $userValue = $session->get($session_var);

        if ($userValue != $value) {

            $message = $this->getOption('message');
            if (!$message) {
                $message = 'The captcha is not valid';
            }

            $validator->appendMessage(new Message($message, $attribute, 'Captcha'));

            return false;
        }

        return true;
    }
}