<?php
/**
 * Created by PhpStorm.
 * User: aboozar
 * Date: 3/3/14
 * Time: 10:28 PM
 */

namespace Tartan;

use Phalcon\DI;

class Tag extends \Phalcon\Tag
{
    static public function gtoj($date, $format = "Y/m/d", $transNumber = true)
    {
        if (!is_int($date)) {
            $date = strtotime($date);
        }
        return jDateTime::date($format, $date, $transNumber, true);
    }

    static public function jtog($date, $format = "Y/m/d")
    {
        return $date;
    }

    static public function icon($name, $isWhite = false, $class = '', $style = "")
    {
        return '<i class="icon-'.$name.' '.$class.'"'.($isWhite ? ' icon-white' : '').' style="'.$style.'"></i>';
    }

    static public function numberFormat($number)
    {
        return number_format($number);
    }

    static public function javascriptIncludePro($url, $relative = true, $attributes = [])
    {
        $baseUri = '';
        if ($relative !== false) {
            $di = DI::getDefault();
            $baseUri = $di->get('url')->getBaseUri();
        }

        $attrs = '';
        foreach ($attributes as $key => &$val) {
            $attrs .= ' '.$key.'="'.$val.'"';
        }

        $tpl = '<script type="text/javascript" src="{path}" {attrs}></script>';
        return strtr($tpl, [
            '{path}'  => $baseUri .$url,
            '{attrs}' => $attrs
        ]);

    }

    static public function hstoreToPhp($string)
    {
        return \Tartan\Helper\Postgres::hstoreToPhp($string);
    }

    static function randomColor() {
        return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
    }

    static function renderSorting($column, $acs)
    {

    }
}