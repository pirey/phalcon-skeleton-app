<?php

namespace Tartan;

use Phalcon\Mvc\User\Component;
use Phalcon\Acl\Adapter\Memory as AclMemory;
use Phalcon\Acl\Role as AclRole;
use Phalcon\Acl\Resource as AclResource;
use Tartan\Acl\Model;

class Acl extends Component
{
    const APC_CACHE_KEY = 'tartan.acl';
    const DEFAULT_ROLE  = 'guest';

    private $_acl;

    private $_cacheFile = 'acl.txt';

    /**
     * Checks if the current profile is allowed to access a resource
     *
     * @param string $role
     * @param string $resource
     * @param string $action
     * @return boolean
     */
    public function isAllowed($role, $resource, $action)
    {
        return $this->getAcl()->isAllowed($role, $resource, $action);
    }

    /**
     * Returns the ACL list
     *
     * @return \Phalcon\Acl\Adapter\Memory
     */
    public function getAcl()
    {
        //Check if the ACL is already created
        if (is_object($this->_acl)) {
            return $this->_acl;
        }

        $acl = $this->cache->get(self::APC_CACHE_KEY);

        if ($this->_acl === null) {
            $acl = $this->rebuild();
        } else {
            $acl = unserialize($acl);
        }

        $this->_acl = $acl;

        return $this->_acl;
    }

    /**
     * Rebuilds the access list into a file
     *
     */
    public function rebuild()
    {
        $acl = new AclMemory();

        $acl->setDefaultAction(\Phalcon\Acl::DENY);

        //Register roles to ACL
        $roles = Model\AclRoles::find();
        foreach ($roles as $role) {
            $acl->addRole(new AclRole($role->name));
        }

        // Register Inherits to ACL
        $inherits = Model\AclRolesInherits::find();
        foreach ($inherits as $inherit) {
            $acl->addInherit($inherit->role_name, $inherit->role_inherit);
        }

        // default role
        //$acl->addResource(new AclResource('default/user_control'), '*');
        //$acl->addRole(new AclRole(self::DEFAULT_ROLE));
        //$acl->allow(self::DEFAULT_ROLE, 'default/user_control', '*');

        // Add resources
        // Grant permissions in "permissions" model
        $permissions = Model\AclPermissions::find();
        foreach ($permissions as $permission)
        {
            if (!$acl->isResource($permission->resource_name)) {
                $acl->addResource(
                    new AclResource($permission->resource_name),$permission->access);
            }

            if ($permission->allowed == 1) {
                $acl->allow($permission->role_name, $permission->resource_name, $permission->access);
            } else {
                $acl->deny($permission->role_name, $permission->resource_name, $permission->access);
            }
        }

        $this->cache->save(self::APC_CACHE_KEY, serialize($acl), (ENV == 'production' ? 3600 : 5));

        return $acl;
    }

}