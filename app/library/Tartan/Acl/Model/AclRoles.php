<?php

namespace Tartan\Acl\Model;

use Tartan\Model,
    Phalcon\Mvc\Model\Relation;

/**
 * AclRoles
 *
 * Stores the permissions by profile
 */
class AclRoles extends Model
{
	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var string
	 */
	public $description;

	public function initialize()
	{
        parent::initialize();
        $this->setSource("acl_roles");

        $this->hasMany('name', 'Tartan\Acl\Model\AclRolesInterits', 'role_name', array(
            'alias' => 'parents',
            'foreignKey' => array(
                'action' => Relation::ACTION_CASCADE
            )
        ));

        $this->hasMany('name', 'Tartan\Acl\Model\AclRolesInterits', 'role_inherit', array(
            'alias' => 'children',
            'foreignKey' => array(
                'action' => Relation::ACTION_CASCADE

            )
        ));
	}

}