<?php

namespace Tartan\Acl\Model;

use Tartan\Model;

/**
 * AclRoles
 *
 * Stores the permissions by profile
 */
class AclRolesInherits extends Model
{
    public $role_name;
    public $role_inherit;

    public function initialize()
    {
        parent::initialize();
        $this->setSource("acl_roles_inherits");

        $this->belongsTo('role_name', 'Tartan\Acl\Model\AclRoles', 'name', array(
            'foreignKey' => TRUE
        ));
        $this->belongsTo('role_inherit', 'Tartan\Acl\Model\AclRoles', 'name', array(
            'foreignKey' => TRUE
        ));
    }

}