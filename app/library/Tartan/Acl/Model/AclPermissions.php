<?php

namespace Tartan\Acl\Model;

use Tartan\Model;

/**
 * AclRoles
 *
 * Stores the permissions by profile
 */
class AclPermissions extends Model
{
    /**
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     * @var string
     */
	public $role_name;

	/**
	 * @var string
	 */
	public $resource_name;
    /**
     * @var string
     */
    public $access;
    /**
     * @var integer
     */
    public $allowed;

	public function initialize()
	{
        parent::initialize();
        $this->setSource("acl_permissions");

        $this->belongsTo('role_name', 'Tartan\Acl\Model\AclRoles', 'name', array(
            'foreignKey' => TRUE
        ));
	}

}