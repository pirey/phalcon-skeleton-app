<?php
namespace App\Entity;

class ErrorCodes
{
    const SERVICE_NOT_FOUND          = 100;
    const SERVICE_UNAVAILABLE        = 101;
    const KEYWORD_ON_WRONG_SHORTCODE = 102;
    const KEYWORD_NOT_FOUND          = 103;
    const INVALID_SHORTCODE          = 104;
    const EMPTY_MESSAGE_CONTENT      = 105;
}