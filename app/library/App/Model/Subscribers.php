<?php
namespace App\Model;
use Phalcon\Mvc\Model\Behavior\Timestampable;

class Subscribers extends AppModel
{

    public $id;
    public $msisdn;
    public $fk_services_id;
    public $flag;
    public $last_message;
    public $autocharge;

    public function initialize ()
    {
        parent::initialize();

        $this->setSource("subscribers");

        $this->addBehavior(new Timestampable(
            [
                'beforeValidationOnCreate' => [
                    'field'  => 'created_at',
                    'format' => 'Y-m-d H:i:s'
                ]
            ]
        ));

        $this->belongsTo("fk_services_id", 'App\Model\Services', "id", [
            'alias' => 'service'
        ]);
    }
}