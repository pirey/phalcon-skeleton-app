<?php
namespace App\Model;
use Phalcon\Mvc\Model\Relation;
use Phalcon\DI;

class AppModel extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
//        $this->setConnectionService('dbMysql');
    }

    public static function _($message)
    {
        return DI::getDefault()->get('t9n')->_($message);
    }
}