<?php
namespace App\Model;

class SubscriptionLogs extends AppModel
{
    public $id;
    public $msisdn;
    public $command;
    public $created_at;

    public function getSource()
    {
        return "subscription_logs";
    }
}