<?php
namespace App\Model;

class Dictionary
{
    const EMPTY_MESSAGE = <<<EMPTY_MESSAGE
مشترک گرامی، برای مشاهده لیست سرویس های متنی و مسابقات ما می توانید به وب سایت {domain} مراجعه کنید
EMPTY_MESSAGE;

    const INVALID_MESSAGE = <<<INVALID_MESSAGE
پیام شما قابل شناسایی توسط نرم افزار نمی باشد! برای دریافت لیست سرویس های پیامکی ما یک پیامک خالی رایگان به شماره {dfshcode} ارسال کنید
INVALID_MESSAGE;
}
