<?php
namespace App\Model;
use Phalcon\Mvc\Model\Validator\InclusionIn;

class Logs extends AppModel
{

    public function getSource()
    {
        return "logs";
    }

    public function validation ()
    {
        $this->validate(new InclusionIn([
            "field"  => "type",
            "domain" => ['debug','info','notice','warning','error','critical','alert','emergency']
        ]));

        return $this->validationHasFailed() != true;
    }

    public function log($type, $message, $entityType, $entityId, $parentId, $inputs = null, $outputs = null)
    {
        $err                 = new self();
        $err->type           = $type;
        $err->entity_type    = $entityType;
        $err->fk_entity_id   = $entityId;
        $err->fk_parent_id   = $parentId;
        $err->message        = $message;
        $err->created_at     = date('Y-m-d H:i:s');
        $err->inputs         = serialize($inputs);
        $err->outputs        = serialize($outputs);
        $err->reported       = 0;
        return ($err->save());
    }


    public function debug ($message, $entityType = null, $entityId = null, $parentId = null, $inputs = null, $outputs = null) {
        return $this->log('debug', $message, $entityType, $entityId, $parentId, $inputs, $outputs);
    }

    public function info ($message, $entityType = null, $entityId = null, $parentId = null, $inputs = null, $outputs = null) {
        return $this->log('info', $message, $entityType, $entityId, $parentId, $inputs, $outputs);
    }

    public function notice ($message, $entityType = null, $entityId = null, $parentId = null, $inputs = null, $outputs = null) {
        return $this->log('notice', $message, $entityType, $entityId, $parentId, $inputs, $outputs);
    }

    public function warning ($message, $entityType = null, $entityId = null, $parentId = null, $inputs = null, $outputs = null) {
        return $this->log('warning', $message, $entityType, $entityId, $parentId, $inputs, $outputs);
    }

    public function error ($message, $entityType = null, $entityId = null, $parentId = null, $inputs = null, $outputs = null) {
        return $this->log('error', $message, $entityType, $entityId, $parentId, $inputs, $outputs);
    }

    public function critical ($message, $entityType = null, $entityId = null, $parentId = null, $inputs = null, $outputs = null) {
        return $this->log('critical', $message, $entityType, $entityId, $parentId, $inputs, $outputs);
    }

    public function alert ($message, $entityType = null, $entityId = null, $parentId = null, $inputs = null, $outputs = null) {
        return $this->log('alert', $message, $entityType, $entityId, $parentId, $inputs, $outputs);
    }

    public function emergency ($message, $entityType = null, $entityId = null, $parentId = null, $inputs = null, $outputs = null) {
        return $this->log('emergency', $message, $entityType, $entityId, $parentId, $inputs, $outputs);
    }
}