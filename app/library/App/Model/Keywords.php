<?php
namespace App\Model;

class Keywords extends AppModel
{
    public $id;
    public $fk_services_id;
    public $txt;

    public function initialize ()
    {
        parent::initialize();

        $this->setSource("keywords");

        $this->belongsTo("fk_services_id", 'App\Model\Services', "id", [
            'alias' => 'service'
        ]);
    }
}