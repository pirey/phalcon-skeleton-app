<?php
namespace App\Model;

use Phalcon\Mvc\Model\Behavior\Timestampable;

class Outbounds extends AppModel
{
    public function getSource()
    {
        return "outbounds";
    }


    public function initialize ()
    {
        parent::initialize();

        $this->addBehavior(new Timestampable(
            [
                'beforeValidationOnCreate' => array(
                    'field'  => 'created_at',
                    'format' => 'Y-m-d H:i:s'
                ),
                'OnUpdate' => array(
                    'field'  => 'updated_at',
                    'format' => 'Y-m-d H:i:s'
                ),
            ]
        ));

        $this->belongsTo("fk_services_id", 'App\Model\Services', "id", [
            'alias' => 'service'
        ]);
    }

    public function beforeValidationOnCreate ()
    {
        if (empty($this->is_free)) {
            $this->is_free = 0;
        }
        if (empty($this->failed)) {
            $this->failed = 0;
        }
        if (empty($this->processed)) {
            $this->processed = 0;
        }
    }
}