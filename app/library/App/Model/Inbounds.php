<?php
namespace App\Model;

use Phalcon\Mvc\Model\Behavior\Timestampable;

class Inbounds extends AppModel
{
    public $id;
    public $fk_outbounds_id;
    public $msisdn;
    public $shortcode;
    public $raw_content;
    public $content;
    public $fk_services_id;
    public $invalid;
    public $processed;
    public $created_at;
    public $updated_at;
    public $shamsi_date;
    public $failed;
    public $autocharge;


    public function getSource ()
    {
        return "inbounds";
    }

    public function initialize ()
    {
        parent::initialize();
        $this->addBehavior(new Timestampable(
            [
                'beforeValidationOnCreate' => [
                    'field'  => 'created_at',
                    'format' => 'Y-m-d H:i:s'
                ],
                'onUpdate' => [
                    'field'  => 'updated_at',
                    'format' => 'Y-m-d H:i:s'
                ],
            ]
        ));

        $this->belongsTo("fk_services_id", 'App\Model\Services', "id", [
            'alias' => 'service'
        ]);
    }

    public function beforeValidationOnCreate ()
    {
        if (empty($this->invalid)) {
            $this->invalid = 0;
        }
        if (empty($this->failed)) {
            $this->failed = 0;
        }
        if (empty($this->processed)) {
            $this->processed = 0;
        }
        if (empty($this->autocharge)) {
            $this->autocharge = 0;
        }
    }

    public function save($data=null, $whiteList=null)
    {
        $r = parent::save();
        if (!$r)
        {
            // log the error
            $log = \Phalcon\DI::getDefault()->get('logger')->emergency(
                'Could not save inbound!', 'inbound', $this->id, $this->fk_services_id, $this->getMessages()
            );

            // notify development
            $this->getDI()->get('notify')->add(
                $this->getDI()->get('config')->notification->developerId,
                'Could not save inbound!',
                'development',
                'inbound',
                '/error/'
            );
        }
        return $r;
    }
}