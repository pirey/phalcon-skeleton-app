<?php
namespace App\Model;
use Phalcon\Mvc\Model\Relation;
use Phalcon\Mvc\Model\Validator\Regex;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use Phalcon\Mvc\Model\Validator\Url;
use Phalcon\Mvc\Model\Validator\Inclusionin;

class Services extends AppModel
{

    public $id;
    public $mci_service_id;
    public $name;
    public $status;
    public $od_url;
    public $ac_url;
    public $cat;
    public $token;
    public $free_shortcode;
    public $cost_shortcode;
    public $dedicated;
    public $slogan;

    public function initialize ()
    {
        parent::initialize();
        $this->setSource("services");

        $this->hasMany("id", 'App\Model\Keywords', "fk_services_id", [
            'alias'      => 'keywords',
            'foreignKey' => [
                'action' => Relation::ACTION_CASCADE
            ]
        ]);

        $this->hasMany("id", 'App\Model\Inbounds', "fk_services_id", [
            'alias'      => 'inbounds',
            'foreignKey' => [
                'action' => Relation::ACTION_CASCADE
            ]
        ]);

        $this->hasMany("id", 'App\Model\Outbounds', "fk_services_id", [
            'alias'      => 'outbounds',
            'foreignKey' => [
                'action' => Relation::ACTION_CASCADE
            ]
        ]);

        $this->hasMany("id", 'App\Model\Subscribers', "fk_services_id", [
            'alias'      => 'subscribers',
            'foreignKey' => [
                'action' => Relation::ACTION_CASCADE
            ]
        ]);
    }


    public function beforeValidationOnCreate ()
    {
        $this->token = sha1(uniqid());
    }

    public function beforeValidationOnUpdate ()
    {
        if (strlen($this->token) < 40) {
            $this->token = sha1(uniqid());
        }
    }

    public function validation ()
    {
        if (!empty($this->dedicated)) {
            $this->validate(new Regex(array(
                'field'   => 'dedicated',
                'pattern' => '/^[0-9]{4,}$/',
                'message' => 'The Dedicated shortcode is invalid.'
            )));

            $this->validate(new Uniqueness([
                "field"   => "dedicated",
                "message" => "The Dedicated shortcode already exists."
            ]));
        }
//        $this->validate(new InclusionIn([
//            "field"  => "status",
//            "domain" => ["normal", "resend"]
//        ]));
//
        $this->validate(new InclusionIn([
            "field"  => "cat",
            "domain" => array_keys(self::Categories())
        ]));
//
        $this->validate(new Url([
            "field"  => "od_url",
            'message' => 'Ondemand URL is invalid'
        ]));

        $this->validate(new Url([
            "field"  => "ac_url",
            'message' => 'Autocharge URL is invalid'
        ]));


        return $this->validationHasFailed() != true;
    }

    public static function Categories ()
    {
        return [
            'unknown'     => self::_('Unknown'),
            'interactive' => self::_('Interactive'),
            'timebased'   => self::_('Timebased'),
            'countbased'  => self::_('Countbased')
        ];
    }
}