<?php

/**
 * Form
 * @copyright Copyright (c) 2011 - 2013 Aleksandr Torosh (http://wezoom.com.ua)
 * @author Aboozar Ghafari <a6oozar@gmail.com>
 */

namespace App\Forms;

use Phalcon\DI;
use \Phalcon\Forms\Element\Hidden;
use \Phalcon\Forms\Element\Check;
use \Phalcon\Forms\Element\Submit;

class Form extends \Phalcon\Forms\Form
{

    public function render($name, $renderError = true)
    {
        $element = $this->get($name);
        $messages = $this->getMessagesFor($name);

        if (count($messages))
        {
            $element->setAttribute('class', $element->getAttribute('class') . ' error');

            $element->setUserOption('group-class', $element->getUserOption('group-class', '').' error');
            $element->setAttribute('class', $element->getAttribute('class'). ' error');
        }
        if ($element->getAttribute('required')) {
            $element->setLabel($this->getLabel($name) .' *');
        }

        $html = parent::render($name);
        if ($renderError) {
            $html .= $this->renderErrorFor($name);
        }

        return $html;
    }

    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }

    public function renderErrorFor($name)
    {
        if (count($this->getMessagesFor($name)) > 0) {
            $html = '';
            foreach ($this->getMessagesFor($name) as $message) {
                $html .= "<li>{$message}</li>";
                break; // Shows only 1 error message
            }

            return strtr('<ul class="parsley-error-list">{items}</ul>', ['{items}' => $html]);

        } else {
            return '';
        }
    }

    public static function _($message)
    {
        return DI::getDefault()->get('t9n')->_($message);
    }
}