<?php
$hostname = gethostname();
switch ($hostname)
{
    case 'ibm':
    case 'lenovo':
    case 'ideacentre':
        define('ENV', 'development');
        break;
    default:
        define('ENV', 'production');
        break;
}

defined('ROOT') || define('ROOT', realpath(__DIR__ . '/..'));

if (ENV == 'development') {
    function d()  {call_user_func_array('var_dump', func_get_args());}
    function dd() {call_user_func_array('var_dump', func_get_args()); die();}
    function ddd() {d("I`m here\n". print_r(func_num_args(), true) ."\n". mt_rand(0, 100)); die();}
} else {
    function d()   {return;}
    function dd()  {return;}
    function ddd() {return;}
}


// inbound messages with processed = 0
define('QUEUE_INBOUNDS_TO_PROCESS', 'platform_q_in_to_process');

// update last message flags for autocharge
define('QUEUE_UPDATE_LAST_MESSAGE', 'platform_q_update_last_message');

// inbound messages that should change their processed flag to 1
define('QUEUE_INBOUNDS_TO_UPDATE_FLAG', 'platform_q_inbounds_to_update_flag');

# processed inbounds that should inject in outbounds mysql table
define('QUEUE_PROCESSED_INBOUNDS', 'platform_q_in_processed_to_move_outbound');

# outbound messages that should change their processed flag to 1
define('QUEUE_OUTBOUNDS_TO_UPDATE_FLAG', 'platform_q_out_to_update_flag');

# outbound messages that should sent to operator API
define('QUEUE_OUTBOUNDS_TO_OPERATOR', 'platform_q_out_to_operator');

define('PLATFORM_PAUSED', 'platform_paused');

define('CONFIG_PICKER_LIMIT', 'config_picker_limit');
define('CONFIG_PICKER_DELAY', 'config_picker_delay');

require_once __DIR__ . '/../vendor/autoload.php';