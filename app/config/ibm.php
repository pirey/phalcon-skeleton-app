<?php

return [
    'application' => [
        'baseUri'       => '/0000/platform/',
        'staticBaseUri' => '/0000/platform/',
        'basePath'      => '/home/aboozar/www/0000/platform/',
    ],
    'databases'    => [
        'db' => [
            'username' => 'root',
            'password' => '1234',
            'dbname'   => 'mci_platform',
        ],
        'dbTartan' => [
            'username' => 'root',
            'password' => '1234',
            'dbname'   => 'skelet',
        ],
    ],
    'mandrill_api_key' => '755bedd4-07e5-4c31-a1a7-b7146b0c2c94',
];
