<?php
$config = [
    'application' => [
        'name'          => 'APP NAME',
        'cacheDir'      => ROOT . '/cache/',
        'appDir'        => ROOT . '/app/',
        'baseUri'       => '/',
        'staticBaseUri' => 'http://cdn.domain.com/',
        'basePath'      => '/var/www/domain.com/',
        'cryptSalt'     => '$9diko$.f#11',
        'defaultFreeShortcode' => 2056,
        'invalidsUrl' => 'http://127.0.0.1/0000/platform/invalid?shortcode={shortcode}&code={code}'
    ],
    'tartan' => [
        'db' => 'dbTartan'
    ],
    'databases'   => [
        'dbTartan' => [
            'adapter'  => 'Phalcon\Db\Adapter\Pdo\Mysql',
            'host'     => 'localhost',
            'username' => 'root',
            'password' => '1234',
            'dbname'   => 'skelet',
            'charset'  => 'utf8',
        ],
        'db' => [
            'adapter'  => 'Phalcon\Db\Adapter\Pdo\Mysql',
            'host'     => 'localhost',
            'username' => 'root',
            'password' => '1234',
            'dbname'   => 'platform',
            'charset'  => 'utf8',
        ]
    ],
    'redis'       => [
        'scheme' => 'tcp',
        'host'   => '127.0.0.1',
        'port'   => 6379
    ],
    'view'        => [
        'viewsDir'      => ROOT . '/app/views/',
        'partialsDir'   => 'partials',
        'layoutsDir'    => 'layouts',
        'defaultLayout' => 'default',
        'pagination'    => 20
    ],
    'assets'      => [
        'dir' => ROOT . '/public/assets/',
    ],
    'cache'       => [
        'prefix' => 'skelet'
    ],
    'mail'        => [
        'fromName'         => 'noreply',
        'fromEmail'        => 'noreply@domain.com',
        'smtp'             => [
            'server'   => 'MAIL_SERVER',
            'port'     => 587,
            'security' => 'tls',
            'username' => 'USERNAME',
            'password' => 'PASSWORD',
        ],
        'mandrill_api_key' => 'MANDRILL',
        'amazon'           => [
            'AWSAccessKeyId' => "ASKID",
            'AWSSecretKey'   => "ASK"
        ],
    ],
    't9n'        => [
        'path'      => ROOT . '/res/t9n/',
        'default'   => 'fa',
        'available' => ['fa', 'en']
    ],
    'auth'  => [
        'defaultRole' => 'guest',
    ],
    'oAuth'       => [
        "Github"    => [ # https://github.com/settings/applications
            'id'     => 'afc32ec3948dc0e2bfea',
            'secret' => '5199f8c0ebb118380e9f04044aa741af234399f3',
        ],
        "Google"    => [ # https://console.developers.google.com/project/apps~hardy-gearing-640/apiui/credential
            'id'     => '67606769472-ls49bh2r041lq8adplsn7ko6ak5avvpk.apps.googleusercontent.com',
            'secret' => 'm3OOGCEDlRPTv6xnm15yuvzn',
        ],
        'Microsoft' => [ # https://account.live.com/developers/applications
            'id'     => '000000004012584B',
            'secret' => 'hbspvkNwtQxE74oQQeJbL6KcmsJLMQr-'
        ],
        'Facebook'  => [ # https://developers.facebook.com/
            'id'     => '1431072327175669',
            'secret' => 'f519b5fa6d8dc9478b261541d7f571f2'
        ]
    ],
    'notification' => [
        'developerId' => 1
    ],
    'api' => [
        'username' => 'Atitel',
        'password' => 'rah@Te123H',
        'wsdl' => 'http://services.hamrahvas.com/SMSBuffer.asmx?wsdl'
    ],
    'company' => [
        'name'   => 'آتی تل',
        'domain' => 'ativas.ir',
        'tel'    => '88759645'
    ]
];

function array_merge_recursive_replace ()
{
    $arrays = func_get_args();
    $base   = array_shift($arrays);

    foreach ($arrays as $array) {
        reset($base);
        while (list($key, $value) = @each($array)) {
            if (is_array($value) && @is_array($base[$key])) {
                $base[$key] = array_merge_recursive_replace($base[$key], $value);
            }
            else {
                $base[$key] = $value;
            }
        }
    }

    return $base;
}

$config = file_exists(__DIR__ . DIRECTORY_SEPARATOR . gethostname() . '.php')
    ? array_merge_recursive_replace($config, require(gethostname() . '.php'))
    : $config;

return new \Phalcon\Config($config);
